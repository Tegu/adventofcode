import fileinput


def sign(value):
    if value < 0:
        return -1
    elif value > 0:
        return 1
    elif value == 0:
        return 0
    else:
        raise AttributeError('Non-numeric value "{}"'.format(value))


def is_report_safe(report):
    assert(len(report) > 1)
    first_sign = sign(report[1] - report[0])
    for i in range(1, len(report)):
        diff = report[i] - report[i - 1]
        if not (1 <= abs(diff) <= 3) or sign(diff) != first_sign:
            return False
    return True


def is_report_almost_safe(report):
    for i in range(len(report)):
        if is_report_safe(report[:i] + report[i+1:]):
            return True
    return False


def part1():
    lines = [[int(level) for level in line.rstrip().split()] for line in fileinput.input()]
    safe_count = sum(is_report_safe(line) for line in lines)
    print('Part 1: {}'.format(safe_count))


def part2():
    lines = [[int(level) for level in line.rstrip().split()] for line in fileinput.input()]
    safe_count = sum(is_report_almost_safe(line) for line in lines)
    print('Part 2: {}'.format(safe_count))



if __name__ == '__main__':
    part1()
    part2()
