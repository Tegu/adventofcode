import copy
import fileinput
import re


REQUIRED_FIELDS = frozenset((
        'byr', # Birth Year
        'iyr', # Issue Year
        'eyr', # Expiration Year
        'hgt', # Height
        'hcl', # Hair Color
        'ecl', # Eye Color
        'pid', # Passport ID
        #'cid', # Country ID
        ))


def parse_passports(fp):
    passports = []
    current_passport = {}
    for line in fp:
        if line.strip() == '':
            passports.append(copy.deepcopy(current_passport))
            current_passport.clear()
        current_passport.update(dict(
            pair.split(':', maxsplit=1)
            for pair
            in line.split()
            ))
    passports.append(copy.deepcopy(current_passport))
    return passports


def has_required_fields(passport):
    return REQUIRED_FIELDS.issubset(set(passport.keys()))


def is_valid_value(key, value):
    if key == 'byr':
        return (value.isdigit()
                and len(value) == 4
                and 1920 <= int(value) <= 2002)
    if key == 'iyr':
        return (value.isdigit()
                and len(value) == 4
                and 2010 <= int(value) <= 2020)
    if key == 'eyr':
        return (value.isdigit()
                and len(value) == 4
                and 2020 <= int(value) <= 2030)
    if key == 'hgt':
        num = value[:-2]
        unit = value[-2:]
        if unit == 'cm' and num.isdigit() and 150 <= int(num) <= 193:
            return True
        elif unit == 'in' and num.isdigit() and 59 <= int(num) <= 76:
            return True
        return False
    if key == 'hcl':
        return bool(re.fullmatch(r'#[0-9a-f]{6}', value))
    if key == 'ecl':
        return value in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth')
    if key == 'pid':
        return value.isdigit() and len(value) == 9
    if key == 'cid':
        return True
    return False


def is_valid_passport(passport):
    if not has_required_fields(passport):
        return False
    for key, value in passport.items():
        if not is_valid_value(key, value):
            return False
    return True


passports = parse_passports(fileinput.input())

valid_count = sum(
        has_required_fields(passport)
        for passport
        in passports
        )
print('Part 1:', valid_count)

valid_count = sum(
        is_valid_passport(passport)
        for passport
        in passports
        )
print('Part 2:', valid_count)
