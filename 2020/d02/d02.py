import collections
import fileinput

Policy = collections.namedtuple('Policy', 'letter lowest highest')
Entry = collections.namedtuple('Entry', 'policy password')

def parse_policy(text):
    lowest = int(text.split('-')[0])
    highest = int(text.split('-')[1].split(' ')[0])
    letter = text.split('-')[1].split(' ')[1]
    return Policy(letter, lowest, highest)


def parse_entry(line):
    policy_str, password = line.split(': ')
    return Entry(parse_policy(policy_str), password)


def part1():
    valid_passwords = 0

    for line in fileinput.input():
        entry = parse_entry(line.rstrip())
        counts = collections.Counter(entry.password)
        policy = entry.policy
        if policy.letter in counts and policy.lowest <= counts[policy.letter] <= policy.highest:
            valid_passwords += 1

    print('Part 1:', valid_passwords)

def part2():
    valid_passwords = 0

    for line in fileinput.input():
        entry = parse_entry(line.rstrip())
        policy = entry.policy
        counts = collections.Counter([entry.password[pos-1] for pos in (policy.lowest, policy.highest)])
        if policy.letter in counts and counts[policy.letter] == 1:
            valid_passwords += 1

    print('Part 2:', valid_passwords)

part1()
part2()
