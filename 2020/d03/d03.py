import math
import fileinput


def count_trees(lines, slope):
    pos = [0, 0]
    trees = 0
    while pos[1] < len(lines):
        line = lines[pos[1]]
        if line[pos[0] % len(line)] == '#':
            trees += 1
        pos[0] += slope[0]
        pos[1] += slope[1]
    return trees


def part1():
    lines = [line.rstrip() for line in fileinput.input()]
    trees = count_trees(lines, (3, 1))
    print('Part 1: {} trees'.format(trees))


def part2():
    lines = [line.rstrip() for line in fileinput.input()]

    slopes = [
            (1, 1),
            (3, 1),
            (5, 1),
            (7, 1),
            (1, 2),
            ]

    slope_trees = {slope: count_trees(lines, slope)
            for slope in slopes}
    print('Part 2: {} trees'.format(math.prod(slope_trees.values())))


if __name__ == '__main__':
    part1()
    part2()
