with open('input.txt') as fp:
    entries = [int(line.rstrip()) for line in fp]

for x in entries:
    for y in entries:
        for z in entries:
            if x + y + z == 2020:
                print('{} * {} * {} = {}'.format(x, y, z, x * y * z))
                break
