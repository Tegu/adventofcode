import fileinput
import collections


BagContainee = collections.namedtuple('BagContainee', 'quantity color')


def remove_bag_postfix(bag):
    # '9 light red bags' -> '9 light red'
    return bag.rsplit(' ', maxsplit=1)[0]


def split_quantity_color(bag):
    # '9 light red' -> (9, 'light red')
    if bag[0].isdigit():
        quantity, color = bag.split(' ', maxsplit=1)
        return BagContainee(int(quantity), color)
    return BagContainee(0, None)


def parse_bag_rules(fp):
    rules = collections.defaultdict(list)
    for line in fp:
        container, containee = line.split(' contain ', maxsplit=1)
        container = remove_bag_postfix(container)
        containee = [
                split_quantity_color(remove_bag_postfix(bag))
                for bag in containee.rstrip('.').split(', ')
                ]
        rules[container].extend(containee)
    return rules


def form_parent_graph(rules):
    parents = collections.defaultdict(list)
    for container, containees in rules.items():
        for containee in containees:
            parents[containee.color].append(container)
    return parents


def get_ascendant_nodes_from(parents, start):
    asc = set()
    stack = [start]
    while stack:
        current = stack.pop()
        if current in parents:
            stack.extend(parents[current])
            asc.update(parents[current])
    return asc


def main():
    rules = parse_bag_rules(fileinput.input())
    parents = form_parent_graph(rules)
    asc = get_ascendant_nodes_from(parents, 'shiny gold')
    print('Part 1:', len(asc))


if __name__ == '__main__':
    main()
