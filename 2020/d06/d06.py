import argparse

parser = argparse.ArgumentParser()
parser.add_argument('filename')
args = parser.parse_args()

with open(args.filename) as fp:
    print('Part 1:', sum(len(set(group.replace('\n', ''))) for group in fp.read().split('\n\n')))

with open(args.filename) as fp:
    print('Part 2:', sum(
        len(set.intersection(*map(set, group.split('\n'))))
        for group
        in fp.read().rstrip().split('\n\n')))
