import fileinput


def binarify(line):
    return line.replace('F', '0').replace('B', '1').replace('L', '0').replace('R', '1')


def parse_seat(line):
    return (
            int(line[:7], base=2),
            int(line[7:], base=2),
            )

def seat_index(seat):
    return seat[0] * 8 + seat[1]

seats = [parse_seat(binarify(line.strip())) for line in fileinput.input()]
seat_indices = [seat_index(seat) for seat in seats]
print('Part 1:', max(seat_indices))
seat_indices.sort()
for i in range(1, len((seat_indices))):
    if seat_indices[i] != 1 + seat_indices[i-1]:
        print('Part 2:', seat_indices[i] - 1)
        break
