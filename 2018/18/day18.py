import argparse
import copy


def neighbors(field, x, y):
    items = []
    for j in range(y-1, y+2):
        for i in range(x-1, x+2):
            if i == x and j == y:
                continue
            items.append(field[j][i])
    return items


def decide_next(current, neigh):
    trees = len(tuple(n for n in neigh if n == "|"))
    lumberyards = len(tuple(n for n in neigh if n == "#"))
    if current == "." and trees >= 3:
        return "|"
    elif current == "|" and lumberyards >= 3:
        return "#"
    elif current == "#":
        if trees >= 1 and lumberyards >= 1:
            return "#"
        else:
            return "."
    return current


def advance(field):
    new = copy.deepcopy(field)
    for y in range(1, len(new)-1):
        for x in range(1, len(new[y])-1):
            current = field[y][x]
            neigh = neighbors(field, x, y)
            new[y][x] = decide_next(current, neigh)
    return new


def count(field, acre):
    trees = 0
    for y in range(1, len(field)-1):
        for x in range(1, len(field[y])-1):
            if field[y][x] == acre:
                trees += 1
    return trees


def print_field(field):
    for y in range(1, len(field)-1):
        for x in range(1, len(field[y])-1):
            print(field[y][x], end="")
        print()
            
def to_tuple(field):
    return tuple(tuple(f) for f in field)


parser = argparse.ArgumentParser()
parser.add_argument("filename")
parser.add_argument("-m", "--minutes", type=int, default=10)
args = parser.parse_args()

with open(args.filename) as fp:
    field = []
    for line in fp:
        field.append(list("." + line.strip() + "."))
    padding = "." * len(field[0])
    field.insert(0, padding)
    field.append(padding)

states = {}
states[to_tuple(field)] = 0
repeat_i = -1
#print_field(field)
for i in range(args.minutes):
    field = advance(field)
    tuplefield = to_tuple(field)
    if tuplefield in states:
        repeat_i = states[tuplefield]
        print("Found repeat after {} minutes. Last seen after {} minutes".format(i+1, repeat_i+1))
        break
    states[tuplefield] = i
    #print("\nAfter {} minutes:".format(i+1))
    #print_field(field)

del states

print("\nAfter {} minutes:".format(i+1))
print_field(field)

trees = count(field, "|")
lumberyards = count(field, "#")
resources = trees * lumberyards
print("Result ({} minutes): {} * {} = {}".format(i+1, trees, lumberyards, resources))
