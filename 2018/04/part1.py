import collections
import datetime
import enum

class EventType(enum.Enum):
    BEGIN_SHIFT = 0
    FALL_ASLEEP = 1
    WAKE_UP = 2

Entry = collections.namedtuple("Entry", ["timestamp", "guard_id", "event_type"])
Sleep = collections.namedtuple("Sleep", ["start_time", "duration", "guard_id"])

def parse_guard(line):
    sharp_i = line.find("#")
    space_i = line.find(" ", sharp_i)
    guard_id = int(line[sharp_i+1: space_i])
    return guard_id

def parse_log_entry(guard, line):
    timestamp = datetime.datetime.strptime(
            line.rsplit("]", 1)[0].strip("[")
            , "%Y-%m-%d %H:%M")
    message = line.split(maxsplit=2)[2].rstrip()
    event_type = None
    if "begins shift" in message:
        event_type = EventType.BEGIN_SHIFT
    elif "falls asleep" in message:
        event_type = EventType.FALL_ASLEEP
    elif "wakes up" in message:
        event_type = EventType.WAKE_UP
    return Entry(timestamp, guard, event_type)

with open("input") as fp:
    entries = []
    current_guard = None
    for line in sorted(fp):
        if "Guard" in line:
            current_guard = parse_guard(line)
        entry = parse_log_entry(current_guard,line)
        entries.append(entry)

sleeps = collections.defaultdict(list)
fell_asleep = None
for entry in entries:
    if entry.event_type == EventType.BEGIN_SHIFT:
        fell_asleep = None
    elif entry.event_type == EventType.FALL_ASLEEP:
        fell_asleep = entry
    elif entry.event_type == EventType.WAKE_UP:
        assert(fell_asleep is not None)
        assert(entry.guard_id == fell_asleep.guard_id)
        duration = entry.timestamp - fell_asleep.timestamp
        sleep = Sleep(fell_asleep.timestamp, duration, entry.guard_id)
        sleeps[entry.guard_id].append(sleep)

max_guard = -1
max_duration = -1
for guard_id in sleeps.keys():
    total = sum(sleep.duration.total_seconds() for sleep in sleeps[guard_id])
    if total > max_duration:
        max_guard = guard_id
        max_duration = total

print(max_guard, int(max_duration/60))

minutes = collections.Counter()
for sleep in sleeps[max_guard]:
    minute_duration = int(sleep.duration.total_seconds()/60)
    for offset in range(minute_duration):
        minute = sleep.start_time.minute + offset
        minutes[minute] += 1

max_minute, occurrences = minutes.most_common(1)[0]
print(max_minute)

print("max_guard * max_minute =", max_guard * max_minute)
