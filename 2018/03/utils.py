import collections

Claim = collections.namedtuple("Claim", ["id", "left", "top", "width", "height"])

def parse_claim(line):
    fields = line.strip().split()
    assert(len(fields) == 4)
    id_ = int(fields[0][1:])
    left, top  = [int(f) for f in fields[2][:-1].split(",")]
    width, height  = [int(f) for f in fields[3].split("x")]
    return Claim(id_, left, top, width, height)

def claim_coordinates(claim):
    for j in range(claim.height):
        for i in range(claim.width):
            yield (claim.left + i, claim.top + j)
