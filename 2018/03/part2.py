import collections

import utils

with open("input") as fp:
    claims = [utils.parse_claim(line) for line in fp]

fabric = collections.Counter()
for claim in claims:
    for x, y in utils.claim_coordinates(claim):
        fabric[(x, y)] +=1

for claim in claims:
    for x, y in utils.claim_coordinates(claim):
        if fabric[(x, y)] > 1:
            break
    else:
        print(claim.id)
