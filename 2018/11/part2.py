import numpy as np

def calc_fuel_grid(width, height, serial_number):
    fuel_grid = np.zeros((height, width))

    it = np.nditer(fuel_grid, flags=["multi_index"], op_flags=["writeonly"])
    while not it.finished:
        rack_id = it.multi_index[1] + 10
        power_level = rack_id * it.multi_index[0] + serial_number
        power_level = power_level * rack_id
        it[0] = (power_level // 100) % 10 - 5
        it.iternext()

    return fuel_grid

def calc_sum_grid(fuel_grid, size):
    sums_shape = np.add(fuel_grid.shape, (-size+1, -size+1))
    sum_grid = np.zeros(sums_shape)

    it = np.nditer(sum_grid, flags=["multi_index"], op_flags=["writeonly"])
    while not it.finished:
        it[0] = fuel_grid[
                    it.multi_index[1]:it.multi_index[1]+size,
                    it.multi_index[0]:it.multi_index[0]+size
                ].sum()
        it.iternext()

    return sum_grid

def find_max(sum_grid):

    max_index = np.unravel_index(np.argmax(sum_grid), sum_grid.shape)
    return sum_grid.max(), max_index


def main():
    import sys
    SERIAL_NUMBER = int(sys.argv[1])
    SIZE = 300
    fuel_grid = calc_fuel_grid(SIZE, SIZE, SERIAL_NUMBER)
    max_size = 1
    max_value = -10
    max_index = None
    for size in range(1, 301):
        print("Size:", size, end=" | ")
        sum_grid = calc_sum_grid(fuel_grid, size)
        value, index = find_max(sum_grid)
        if value > max_value:
            max_value = value
            max_index = index
            max_size = size
        print(max_value, max_index, max_size)

if __name__ == "__main__":
    main()
