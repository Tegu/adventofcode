import numpy as np

def calc_fuel_grid(width, height, serial_number):
    fuel_grid = np.zeros((height, width))

    it = np.nditer(fuel_grid, flags=["multi_index"], op_flags=["writeonly"])
    while not it.finished:
        rack_id = it.multi_index[1] + 10
        power_level = rack_id * it.multi_index[0] + serial_number
        power_level = power_level * rack_id
        it[0] = (power_level // 100) % 10 - 5
        it.iternext()

    return fuel_grid

def calc_sum_grid(fuel_grid):
    sums_shape = np.add(fuel_grid.shape, (-2, -2))
    sum_grid = np.zeros(sums_shape)

    it = np.nditer(sum_grid, flags=["multi_index"], op_flags=["writeonly"])
    while not it.finished:
        it[0] = fuel_grid[
                    it.multi_index[1]:it.multi_index[1]+3,
                    it.multi_index[0]:it.multi_index[0]+3
                ].sum()
        it.iternext()

    return sum_grid

def find_max_index(sum_grid):
    return np.unravel_index(np.argmax(sum_grid), sum_grid.shape)


def main():
    import sys
    SERIAL_NUMBER = int(sys.argv[1])
    WIDTH = 300
    HEIGHT = 300
    fuel_grid = calc_fuel_grid(WIDTH, HEIGHT, SERIAL_NUMBER)
    sum_grid = calc_sum_grid(fuel_grid)
    max_index = find_max_index(sum_grid)
    print(max_index)

if __name__ == "__main__":
    main()
