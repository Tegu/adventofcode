import argparse
import collections
import copy
import itertools
import operator
import sys

def name_to_duration(name):
    return 60 + ord(name) - ord("A") + 1

class Task:
    def __init__(self, name, duration):
        self.name = name
        self.duration = duration
        self.progress = duration

def find_free_worker(workers):
    if None in workers:
        return workers.index(None)
    return -1

def update_workers(workers):
    finished_tasks = []
    for i, task in enumerate(workers):
        if task:
            task.progress -= 1
            if task.progress <= 0:
                finished_tasks.append(task)
                workers[i] = None
    return finished_tasks

def are_workers_active(workers):
    return not all(task is None for task in workers)


parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

nodes = set()
edges = collections.defaultdict(set)

with open(args.filename) as fp:
    for line in fp:
        fields = line.split()
        node_from = fields[1]
        node_to = fields[7]
        edges[node_from].add(node_to)
        nodes.add(node_from)
        nodes.add(node_to)

print("edges", edges, file=sys.stderr)

parent_counts = collections.Counter()
for _to in itertools.chain(*edges.values()):
    parent_counts[_to] += 1

print("parent_counts", parent_counts, file=sys.stderr)

root_nodes = []
for name in edges.keys():
    if name not in parent_counts:
        root_nodes.append(name)

workers = [None]*5
done_nodes = []
t = 0
while len(root_nodes) > 0 or are_workers_active(workers):
    print("{}\tdone_nodes: {}".format(t, repr("".join(done_nodes))), file=sys.stderr)
    while find_free_worker(workers) >= 0 and len(root_nodes) > 0:
        index = find_free_worker(workers)
        min_index, _ = min(enumerate(root_nodes), key=operator.itemgetter(1))
        node = root_nodes.pop(min_index)
        task = Task(node, name_to_duration(node))
        workers[index] = task
    finished_tasks = update_workers(workers)
    for finished_task in finished_tasks:
        node = finished_task.name
        done_nodes.append(node)
        for child in copy.deepcopy(edges[node]):
            edges[node].discard(child)
            parent_counts[child] -= 1
            if parent_counts[child] == 0:
                root_nodes.append(child)
    t += 1

print("".join(done_nodes))
print("Finished in time {}".format(t))

