import argparse
import collections
import copy
import itertools
import operator
import sys


parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

edges = collections.defaultdict(set)

with open(args.filename) as fp:
    for line in fp:
        fields = line.split()
        node_from = fields[1]
        node_to = fields[7]
        edges[node_from].add(node_to)

parent_counts = collections.Counter()
for _to in itertools.chain(*edges.values()):
    parent_counts[_to] += 1

print("parent_counts", parent_counts, file=sys.stderr)

root_nodes = []
for name in edges.keys():
    if name not in parent_counts:
        root_nodes.append(name)

print("root_nodes", root_nodes, file=sys.stderr)

done_nodes = []
while len(root_nodes) > 0:
    min_index, _ = min(enumerate(root_nodes), key=operator.itemgetter(1))
    node = root_nodes.pop(min_index)
    done_nodes.append(node)
    for child in copy.deepcopy(edges[node]):
        edges[node].discard(child)
        parent_counts[child] -= 1
        if parent_counts[child] == 0:
            root_nodes.append(child)
print("".join(done_nodes))
