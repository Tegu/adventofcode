import argparse
import operator
import sys

parser = argparse.ArgumentParser()
parser.add_argument("filename")
parser.add_argument("generations", type=int, default=20, nargs="?")
parser.add_argument("-e", "--extrapolate", type=int, default=50000000000, nargs="?")
args = parser.parse_args()


def parse_state(line):
    return line.split()[-1]


def parse_rule(line):
    return operator.itemgetter(0, -1)(line.split())


def advance_generation(state, rules):
    # assumes that two first and last do not change
    yield state[0]
    yield state[1]
    for i in range(2, len(state)-2):
        yield rules.get(state[i-2:i+3], ".")
    yield state[-2]
    yield state[-1]


def add_padding(state, n):
    return "." * n + state + "." * n


def plant_indices(state, offset):
    return (i - offset for i, v in enumerate(state) if v == "#")


def sum_plant_indices(state, offset):
    return sum(plant_indices(state, offset))


with open(args.filename) as fp:
    initial_state = parse_state(fp.readline())
    print(tuple(plant_indices(initial_state, 0)))
    fp.readline()  # empty line
    rules = dict(parse_rule(line) for line in fp)

print("Initial state:", initial_state, file=sys.stderr)
print("Rules:", rules, file=sys.stderr)
print("Generations:", args.generations, file=sys.stderr)

offset = 2 * args.generations + 2
state = add_padding(initial_state, offset)
for i in range(args.generations):
    print(i, state, sep="\t", file=sys.stderr)
    state = "".join(advance_generation(state, rules))
print(i+1, state, sep="\t", file=sys.stderr)
index_sum = sum_plant_indices(state, offset)
print("Part 1:", index_sum)

indices = tuple(plant_indices(state, offset))
future_sum =  index_sum + len(indices) * (args.extrapolate - args.generations)
print("Part 2:", future_sum)
