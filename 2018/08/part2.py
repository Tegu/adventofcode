import argparse
import itertools


class Node:
    def __init__(self):
        self.children = []
        self.metadata = []


def read_word(fp):
    word = ""
    while True:
        char = fp.read(1)
        if char in " \n":
           break
        word += char
    return word


def read_integer(fp):
    word = read_word(fp)
    return int(word)


def read_children(fp, n):
    children = []
    for i in range(n):
        child = read_node(fp)
        children.append(child)
    return children


def read_metadata(fp, n):
    metadata = []
    for i in range(n):
        entry = read_integer(fp)
        metadata.append(entry)
    return metadata


def read_node(fp):
    node = Node()
    child_count = read_integer(fp)
    meta_count = read_integer(fp)
    node.children = read_children(fp, child_count)
    node.metadata = read_metadata(fp, meta_count)
    return node


def calculate_value(node):
    if len(node.children) == 0:
        return sum(node.metadata)
    value = 0
    for entry in node.metadata:
        if 0 < entry < len(node.children) + 1:
            child = node.children[entry - 1]
            value += calculate_value(child)
    return value


parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

with open(args.filename) as fp:
    tree = read_node(fp)

print("Total:", calculate_value(tree))
