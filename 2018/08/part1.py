import argparse
import itertools


class Node:
    def __init__(self):
        self.children = []
        self.metadata = []


def read_word(fp):
    word = ""
    while True:
        char = fp.read(1)
        if char in " \n":
           break
        word += char
    return word


def read_integer(fp):
    word = read_word(fp)
    return int(word)


def read_children(fp, n):
    children = []
    for i in range(n):
        child = read_node(fp)
        children.append(child)
    return children


def read_metadata(fp, n):
    metadata = []
    for i in range(n):
        entry = read_integer(fp)
        metadata.append(entry)
    return metadata


def read_node(fp):
    node = Node()
    child_count = read_integer(fp)
    meta_count = read_integer(fp)
    node.children = read_children(fp, child_count)
    node.metadata = read_metadata(fp, meta_count)
    return node


def sum_metadata(node):
    count = sum(node.metadata)
    for child in node.children:
        count += sum_metadata(child)
    return count


parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

with open(args.filename) as fp:
    tree = read_node(fp)

print("Total:", sum_metadata(tree))
