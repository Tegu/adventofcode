import argparse
import collections
import copy
import sys


def vecsum(v1, v2):
    return (v1[0] + v2[0], v1[1] + v2[1])


def rotate90(direction, cw):
    if cw:
        return (direction[1], -direction[0])
    return (-direction[1], direction[0])


def get_piece(track, x, y):
    assert(y >= 0 and y < len(track))
    assert(x >= 0 and x < len(track[y]))
    return track[y][x]


class Cart:
    def __init__(self, position, direction):
        self.position = position
        self.direction = direction
        self.next_action = 0

    def update(self):
        self.position = vecsum(self.position, self.direction)

    def turn(self):
        if self.next_action == 0:
            self.direction = rotate90(self.direction, True)
        elif self.next_action == 2:
            self.direction = rotate90(self.direction, False)
        self.next_action = (self.next_action + 1) % 3


def parse_carts(track):
    cart_directions = {
        ">": (1, 0),
        "v": (0, 1),
        "^": (0, -1),
        "<": (-1, 0),
    }
    carts = []
    for y, line in enumerate(track):
        for symbol in "<v^>":
            x = -1
            while True:
                x = line.find(symbol, x + 1)
                if x < 0:
                    break
                position = (x, y)
                direction = cart_directions[symbol]
                cart = Cart(position, direction)
                carts.append(cart)
    return carts


def clear_carts(track):
    for i in range(len(track)):
        for symbol in "<>":
            track[i] = track[i].replace(symbol, "-")
        for symbol in "v^":
            track[i] = track[i].replace(symbol, "|")


def print_map(track, carts):
    direction_carts = {
        (1, 0): ">",
        (0, 1): "v",
        (0, -1): "^",
        (-1, 0): "<",
    }
    for y in range(len(track)):
        for x in range(len(track[y])):
            cur_carts = []
            for cart in carts:
                if cart.position == (x, y):
                    cur_carts.append(cart)
            if len(cur_carts) > 1:
                print("X", end="")
            elif len(cur_carts) == 1:
                print(direction_carts[cur_carts[0].direction], end="")
            elif len(cur_carts) == 0:
                if track[y][x] in "|-":
                    print(" ", end="")
                else:
                    print(track[y][x], end="")
        print()
                


parser = argparse.ArgumentParser()
parser.add_argument("filename")
parser.add_argument("-b", "--backtrace", type=int, default=0)
args = parser.parse_args()

transitions = {
    "/": {
        (-1,0): (0,1),
        (0,-1): (1,0),
        (0,1): (-1,0),
        (1,0): (0,-1)
    },
    "\\": {
        (-1,0): (0,-1),
        (0,-1): (-1,0),
        (0,1): (1,0),
        (1,0): (0,1)
    }
}


with open(args.filename) as fp:
    track = [line.rstrip("\n") for line in fp]
carts = parse_carts(track)
clear_carts(track)

backtrace = collections.deque(maxlen=args.backtrace)

i = 0
while len(carts) > 1:
    to_remove = []
    for cart in sorted(carts, key=lambda c:(c.position[1], c.position[0])):
        cart.update()
        piece = get_piece(track, *cart.position) 
        if piece in "/\\":
            cart.direction = transitions[piece][cart.direction]
        elif piece == "+":
            cart.turn()
        for other_cart in carts:
            if cart is not other_cart\
                    and cart.position == other_cart.position:
                for btrack, bcarts in backtrace:
                    print_map(btrack, bcarts)
                    print("-"*80)
                #print_map(track, carts)
                print("\n[{time}] COLLISION of {c1} and {c2} at {pos[0]},{pos[1]}".format(time=i, c1=carts.index(cart), c2=carts.index(other_cart), pos=cart.position))
                to_remove.append(cart)
                to_remove.append(other_cart)
        for rm_cart in to_remove:
            if rm_cart in carts:
                carts.remove(rm_cart)
    backtrace.append((copy.deepcopy(track), copy.deepcopy(carts)))
    i += 1

if len(carts) > 0:
    print("Last cart at {pos[0]},{pos[1]}".format(pos=carts[0].position))
else:
    print("Total annihilation: No carts left")
