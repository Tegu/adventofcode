import argparse
import collections
import enum


class Directions(enum.IntEnum):
    NORTH = 1 << 0
    EAST = 1 << 1
    SOUTH = 1 << 2
    WEST = 1 << 3


def char_to_dir(dirchars, char):
    return Directions(1 << dirchars.index(char))


def opposite_direction(direction):
    return (direction << 2) % 0b1111


def dir_to_complex(direction):
    if direction == Directions.NORTH:
        return complex(0, -1)
    if direction == Directions.EAST:
        return complex(1, 0)
    if direction == Directions.SOUTH:
        return complex(0, 1)
    if direction == Directions.WEST:
        return complex(-1, 0)
    raise ValueError("Invalid direction")


def move(pos, direction):
    return pos + dir_to_complex(direction)


def create_doors(world, pos, direction):
    world[pos] |= direction
    next_pos = pos + dir_to_complex(direction)
    opposite = opposite_direction(direction)
    world[next_pos] |= opposite_direction(direction)


def generate_world(regex):
    world = collections.defaultdict(int)
    dirchars = "NESW"
    pos_stack = []
    pos = complex(0, 0)
    for c in regex:
        #print(pos)
        if c in dirchars:
            #print("MOVE", c)
            direction = char_to_dir(dirchars, c)
            create_doors(world, pos, direction)
            pos = move(pos, direction)
        elif c == "(":
            # push to stack
            #print("PUSH {} (stack before: {})".format(c, pos_stack))
            pos_stack.append(pos)
        elif c == ")":
            # pop from stack
            #print("POP {} (stack before: {})".format(c, pos_stack))
            assert(len(pos_stack) > 0)
            pos = pos_stack.pop()
        elif c == "|":
            # peek the stack but don't pop
            #print("PEEK", c)
            assert(len(pos_stack) > 0)
            pos = pos_stack[-1]
        elif c == "$":
            break
    return world


def bfs(world):
    visited = set()
    queue = collections.deque()
    queue.append(complex(0, 0))
    distances = {}
    distances[complex(0, 0)] = 0
    while len(queue) > 0:
        pos = queue.popleft()
        if pos in visited:
            continue
        visited.add(pos)
        #print(pos)
        for direction in Directions:
            if world[pos] & direction:
                new_pos = move(pos, direction)
                if new_pos not in distances:
                    distances[new_pos] = distances[pos] + 1
                queue.append(new_pos)
    return distances



parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

with open(args.filename) as fp:
    regex = fp.readline().strip()


world = generate_world(regex)

#print("World")
#for pos, dirs in world.items():
    #print("({0.real}, {0.imag}):\t{1:0>4}".format(pos, bin(dirs)[2:]))

#print("BFS")
distances = bfs(world)
maxdist = max(distances.items(), key=lambda x: x[1])
print("Part 1: Max distance {}".format(maxdist))

part2 = len(tuple(d for d in distances.values() if d >= 1000))
print("Part 2: {}".format(part2))
