import argparse
import itertools
import sys

import node


def play_game(player_count, last_marble):
    print("Players: {}, Last marble score: {}".format(player_count, last_marble))
    current_marble = node.Node(0)
    scores = [0] * player_count
    for marble, player in zip(
            range(1, last_marble + 1),
            itertools.cycle(range(player_count))):
        #print("{} ({}) {}".format(current_marble.previous().number, current_marble.number, current_marble.next().number), file=sys.stderr)
        if marble % 23 == 0:
            score_marble = current_marble.previous(7)
            current_marble = score_marble.next()
            score_marble.pop()
            scores[player] += marble
            scores[player] += score_marble.number
        else:
            current_marble = current_marble.next(2).insert_number(marble)
    print("Winning score:", max(scores))


parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

with open(args.filename) as fp:
    for line in fp:
        fields = line.split()
        assert(len(fields) == 8)
        player_count = int(fields[0])
        last_marble = int(fields[6])
        play_game(player_count, last_marble)
