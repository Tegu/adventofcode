class Node:
    def __init__(self, number):
        self.number = number
        self._previous = self
        self._next = self


    def insert(self, new_node):
        new_node._next = self
        new_node._previous = self._previous
        self._previous._next = new_node
        self._previous = new_node
        return new_node


    def insert_number(self, number):
        return self.insert(Node(number))


    def pop(self):
        self._previous._next = self._next
        self._next._previous = self._previous
        self._previous = self
        self._next = self
        return self


    def previous(self, n=1):
        new_node = self
        for _ in range(n):
            new_node = new_node._previous
        return new_node


    def next(self, n=1):
        new_node = self
        for _ in range(n):
            new_node = new_node._next
        return new_node
