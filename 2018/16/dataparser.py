import collections


Instruction = collections.namedtuple("Instruction",
        ["op", "ri1", "ri2", "ro"])
Sample = collections.namedtuple("Sample",
        ["before", "instruction", "after"])


def read_instruction(fp):
    fields = fp.readline().split()
    if len(fields) == 4:
        ints = tuple(map(int, fields))
        return Instruction(*ints)
    return None


def read_register_contents(fp):
    fields = fp.readline().split(maxsplit=1)
    if len(fields) == 2:
        # ignore whether it's before or after
        registers = tuple(int(r.strip(" ,")) for r in fields[1].strip("[]\n ").split())
        return registers
    return None


def read_sample(fp):
    registers_before = read_register_contents(fp)
    if not registers_before:
        return None
    instruction = read_instruction(fp)
    if not instruction:
        return None
    registers_after = read_register_contents(fp)
    if not registers_after:
        return None
    return Sample(registers_before, instruction, registers_after)



def read_samples(fp):
    samples = []
    bad_counter = 0
    while True:
        sample = read_sample(fp)
        if sample:
            bad_counter = 0
            samples.append(sample)
        else:
            bad_counter += 1
            if bad_counter > 2:
                break
            continue
    return samples


def read_instructions(fp):
    instructions = []
    while True:
        instruction = read_instruction(fp)
        if not instruction:
            break
        instructions.append(instruction)
    return instructions
