import argparse
import sys
import collections

import dataparser
import instructions


def find_matching_functions(sample):
    funcs = []
    op, ri1, ri2, ro = sample.instruction
    for instruction in instructions.collection:
        result = instruction(sample.before, ri1, ri2, ro)
        if result == sample.after:
            funcs.append(instruction)
    return funcs


def find_opcode_candidates(samples):
    candidates = collections.defaultdict(lambda: set(instructions.collection))
    for sample in samples:
        funcs = find_matching_functions(sample)
        op = sample.instruction.op
        candidates[op] = candidates[op].intersection(tuple(funcs))
    return candidates


def reduce_candidates(candidates):
    locked = {}
    while len(locked) < len(candidates):
        for op, funcs in candidates.items():
            if len(funcs) == 1:
                locked[op] = tuple(funcs)[0]
        for op, func in locked.items():
            for op, funcs in candidates.items():
                if len(funcs) > 1 and func in funcs:
                    funcs.remove(func)
#        print("-" * 79)
#        for op, funcs in candidates.items():
#            print(op, [f.__name__ for f in funcs])
    return locked


def run_instruction(registers, instruction, op_instr):
    func = op_instr[instruction.op]
    return func(registers,
            instruction.ri1,
            instruction.ri2,
            instruction.ro)



parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

with open(args.filename) as fp:
    samples = dataparser.read_samples(fp)
    program = dataparser.read_instructions(fp)
    if samples is None or program is None:
        print("Error when reading input", file=sys.stderr)
        sys.exit(1)

#print("\n".join(str(s) for s in samples))
print(samples[-1])
print(program[:3])

candidates = find_opcode_candidates(samples)
op_instr = reduce_candidates(candidates)
registers = (0, 0, 0, 0)
for instruction in program:
    print(registers, instruction)
    registers = run_instruction(registers, instruction, op_instr)
print(registers)
