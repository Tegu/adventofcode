collection = []

# Helper decorator
# The instruction function itself only has to modify the
# registers but the return value is a tuple still
def instruction(func):
    def wrapper(registers, A, B, C):
        working = list(registers)
        func(working, A, B, C)
        return tuple(working)
    wrapper.__name__ = func.__name__
    collection.append(wrapper)
    return wrapper


# Addition:

# addr (add register) stores into register C the result of adding register A and register B.
@instruction
def addr(registers, A, B, C):
    registers[C] = registers[A] + registers[B]

# addi (add immediate) stores into register C the result of adding register A and value B.
@instruction
def addi(registers, A, B, C):
    registers[C] = registers[A] + B


# Multiplication:

# mulr (multiply register) stores into register C the result of multiplying register A and register B.
@instruction
def mulr(registers, A, B, C):
    registers[C] = registers[A] * registers[B]

# muli (multiply immediate) stores into register C the result of multiplying register A and value B.
@instruction
def muli(registers, A, B, C):
    registers[C] = registers[A] * B


# Bitwise AND:

# banr (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
@instruction
def banr(registers, A, B, C):
    registers[C] = registers[A] & registers[B]

# bani (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
@instruction
def bani(registers, A, B, C):
    registers[C] = registers[A] & B


# Bitwise OR:

# borr (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
@instruction
def borr(registers, A, B, C):
    registers[C] = registers[A] | registers[B]

# bori (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
@instruction
def bori(registers, A, B, C):
    registers[C] = registers[A] | B


# Assignment:

# setr (set register) copies the contents of register A into register C. (Input B is ignored.)
@instruction
def setr(registers, A, B, C):
    registers[C] = registers[A]

# seti (set immediate) stores value A into register C. (Input B is ignored.)
@instruction
def seti(registers, A, B, C):
    registers[C] = A


# Greater-than testing:

# gtir (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
@instruction
def gtir(registers, A, B, C):
    registers[C] = 1 if A > registers[B] else 0

# gtri (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
@instruction
def gtri(registers, A, B, C):
    registers[C] = 1 if registers[A] > B else 0

# gtrr (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
@instruction
def gtrr(registers, A, B, C):
    registers[C] = 1 if registers[A] > registers[B] else 0


# Equality testing:

# eqir (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
@instruction
def eqir(registers, A, B, C):
    registers[C] = 1 if A == registers[B] else 0

# eqri (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
@instruction
def eqri(registers, A, B, C):
    registers[C] = 1 if registers[A] == B else 0

# eqrr (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
@instruction
def eqrr(registers, A, B, C):
        registers[C] = 1 if registers[A] == registers[B] else 0
