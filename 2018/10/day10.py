import argparse
import itertools

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot
import numpy


parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

points = []
velocities = []

with open(args.filename) as fp:
    for line in fp:
        fields = line.replace("<", ",").replace(">", ",").rstrip().split(",")
        px = int(fields[1].strip())
        py = int(fields[2].strip())
        vx = int(fields[4].strip())
        vy = int(fields[5].strip())
        points.append((px, py))
        velocities.append((vx, vy))

p = numpy.array(points)
v = numpy.array(velocities)

vari = numpy.Infinity
t_min = 0

for t in itertools.count():
    prev_vari = vari
    vari = numpy.var(p + t*v)
    if vari > prev_vari:
        t_min = t-1
        break

p_min = p + t_min*v
print(t_min)
matplotlib.pyplot.figure(figsize=(4,1))
matplotlib.pyplot.plot(p_min[:,0], -p_min[:,1], ".")
matplotlib.pyplot.savefig("min_points.png")
