import itertools
seen = set()
with open("input") as fp:
	freq = 0
	changes = [int(x) for x in fp.read().strip().split()]
	for change in itertools.cycle(changes):
		freq += change
		if freq in seen:
			print(freq)
			break
		seen.add(freq)
