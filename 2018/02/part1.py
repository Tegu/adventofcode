import collections
import functools
import operator
NUMS = (2,3)
num_counts = collections.Counter()
with open("input") as fp:
	for line in fp:
		letter_counts = collections.Counter(line.strip())
		for num in NUMS:
			if num in letter_counts.values():
				num_counts[num] += 1
product = functools.reduce(operator.mul, num_counts.values())
print(product)
