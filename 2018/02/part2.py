import collections
import functools
import itertools
import operator

def hamming_distance(s1, s2):
	if len(s1) != len(s2):
		raise ValueError()
	return sum(itertools.starmap(operator.ne, zip(s1, s2)))

assert(hamming_distance("ameeba", "ameeba") == 0)
assert(hamming_distance("ameeba", "ameebx") == 1)
assert(hamming_distance("axeeba", "ameeba") == 1)
assert(hamming_distance("axxeba", "ameeba") == 2)
assert(hamming_distance("axxxba", "ameeba") == 3)
assert(hamming_distance("axxxxa", "ameeba") == 4)


with open("input") as fp:
	lines = fp.read().strip().split()
	for line1 in lines:
		for line2 in lines:
			if hamming_distance(line1, line2) == 1:
				print(line1, line2)
				break
