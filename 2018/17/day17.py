import argparse
import collections
import enum

import matplotlib.image
import numpy

class Tile(enum.IntEnum):
    SAND = 0
    CLAY = 1
    WATER_RUN = 2
    WATER_REST = 3
    SPRING = 4

Box = collections.namedtuple("Box", ["xmin", "ymin", "xmax", "ymax"])
Hit = collections.namedtuple("Hit", ["type_", "x", "y"])


def parse_box(line):
    fields = line.replace(",", " ").replace("=", " ").split()
    assert(len(fields) == 4)
    ax1 = fields[0]
    v1 = int(fields[1])
    ax2 = fields[2]
    v2min, v2max = [int(f) for f in fields[3].split("..")]
    if ax1 == "x":
        box = Box(v1, v2min, v1, v2max)
    elif ax1 == "y":
        box = Box(v2min, v1, v2max, v1)
    return box


def add_box(world, box, limits):
    for y in range(box.ymin, box.ymax+1):
        for x in range(box.xmin, box.xmax+1):
            lx, ly = world_to_local(x, y, limits)
            world[ly][lx] = Tile.CLAY


def calc_world_limits(boxes, hpadding):
    xmin = min(box.xmin for box in boxes) - hpadding
    ymin = 0
    xmax = max(box.xmax for box in boxes) + hpadding
    ymax = max(box.ymax for box in boxes)
    return Box(xmin, ymin, xmax, ymax)


def create_world(limits):
    width = limits.xmax - limits.xmin + 1
    height = limits.ymax - 0 + 1
    world = numpy.full((height, width), Tile.SAND, dtype=numpy.int8)
    return world


def fill_world(world, boxes):
    for box in boxes:
        add_box(world, box, limits)


def world_str(world):
    mapping = ".#|~+"
    assert(len(mapping) == len(Tile))
    s = ""
    for y in range(world.shape[0]):
        if y > 0:
            s += "\n"
        for x in range(world.shape[1]):
            s += mapping[int(world[y][x])]
    return s


def world_to_local(x, y, limits):
    return x - limits.xmin, y - limits.ymin


def fill_rect(world, char, x1, y1, x2, y2):
    xmin = min(x1, x2)
    xmax = max(x1, x2)
    ymin = min(y1, y2)
    ymax = max(y1, y2)
    for py in range(ymin, ymax+1):
        for px in range(xmin, xmax+1):
            world[py][px] = char


def count_type(world, type_):
    return numpy.count_nonzero(world == type_)


def search_horizontal(world, x, y, direction):
    px = x
    while 0 < px < world.shape[1] - 1:
        if world[y][px] == Tile.SAND and world[y+1][px] == Tile.SAND:
            return Tile.SAND, px
        if world[y][px] == Tile.WATER_RUN and world[y+1][px] == Tile.WATER_RUN:
            return Tile.WATER_RUN, px
        elif world[y][px] == Tile.CLAY:
            return world[y][px], px - direction
        px += direction
    return None, 0


def search_level(world, x, y):
    hits = []
    for direction in (-1, 1):
        type_, px = search_horizontal(world, x, y, direction)
        hits.append(Hit(type_, px, y))
    return hits


def fill_levels(world, x, y):
    falling_points = []
    py = y
    while True:
        hits = search_level(world, x, py)
        #print("Hits ({}):".format(py), hits)
        for hit in hits:
            if hit.type_ in (Tile.SAND, Tile.WATER_RUN):
                #print("New falling point ({}, {})".format(hit.x, hit.y))
                falling_points.append((hit.x, hit.y))
        minx = min(hit.x for hit in hits)
        maxx = max(hit.x for hit in hits)
        if len(falling_points) > 0:
            fill_rect(world, Tile.WATER_RUN, minx, hit.y, maxx, hit.y)
            break
        else:
            fill_rect(world, Tile.WATER_REST, minx, hit.y, maxx, hit.y)
        py -= 1
    return falling_points


def fall_down(world, x, y):
    assert(0 < y < world.shape[0])
    for py in range(y, world.shape[0]):
        if world[py][x] != Tile.SAND:
            return world[py][x], py
    return None, py + 1


def simulate(world, start_x, start_y):
    falling_points = [(start_x, start_y)]
    while len(falling_points) > 0:
        x, y = falling_points.pop()
        type_, py = fall_down(world, x, y + 1)
        fill_rect(world, Tile.WATER_RUN, x, y, x, py - 1)
        #print("Fell from ({}, {}) to ({}, {})".format(x, y, x, py - 1))
        if type_ in (Tile.CLAY, Tile.WATER_REST):
            # hit clay
            new_points = fill_levels(world, x, py - 1)
            falling_points.extend(new_points)
            #print("Falling points", len(falling_points))


parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

boxes = []

with open(args.filename) as fp:
    for line in fp:
        box = parse_box(line)
        boxes.append(box)

hpadding = 2
limits = calc_world_limits(boxes, hpadding)
world = create_world(limits)
fill_world(world, boxes)

spring_x, spring_y = world_to_local(500, 0, limits)
world[spring_y][spring_x] = Tile.SPRING

#print(world_str(world))
simulate(world, spring_x, spring_y + 1)

print(world_str(world))
matplotlib.image.imsave("17.png", world)
min_boxy = min(box.ymin for box in boxes) - 1
run_count = count_type(world, Tile.WATER_RUN)
rest_count = count_type(world, Tile.WATER_REST)
print("Part 1 count:", run_count + rest_count - min_boxy)
print("Part 2 count:", rest_count)
