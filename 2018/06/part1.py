import argparse
import collections

Target = collections.namedtuple("Target", ["id", "point"])
PointCost = collections.namedtuple("PointCost", ["id", "cost"])


def manhattan_distance(point1, point2):
    return abs(point2.real - point1.real) + abs(point2.imag - point1.imag)


def load_targets(fp):
    targets = []
    for index, line in enumerate(fp):
        coords = [int(num) for num in line.strip().split(", ")]
        target = Target(index, complex(*coords))
        targets.append(target)
    return targets


parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

with open(args.filename) as fp:
    targets = load_targets(fp)

x_min = int(min(t.point.real for t in targets))
x_max = int(max(t.point.real for t in targets))
y_min = int(min(t.point.imag for t in targets))
y_max = int(max(t.point.imag for t in targets))

print(x_min, x_max, y_min, y_max)

graph = {}
for y in range(y_min, y_max+1):
    for x in range(x_min, x_max+1):
        for target in targets:
            point = complex(x, y)
            cost = manhattan_distance(point, target.point)
            if point not in graph or cost < graph[point].cost:
                graph[point] = PointCost(target.id, cost)
            elif cost == graph[point].cost:
                graph[point] = PointCost(None, cost)

forbidden = set()
for y in range(y_min, y_max+1):
    for x in (x_min, x_max):
        point = complex(x, y)
        forbidden.add(graph[point].id)
for x in range(x_min, x_max+1):
    for y in (y_min, y_max):
        point = complex(x, y)
        forbidden.add(graph[point].id)

print("forbidden", forbidden)

counts = collections.Counter()
for point_cost in graph.values():
    counts[point_cost.id] += 1

for point_id in forbidden:
    del counts[point_id]

for k, v in counts.items():
    print(k, v)

max_id, max_area = max(counts.items(), key=lambda x: x[1])
print("Largest area = {} (id {})".format(max_area, max_id))
