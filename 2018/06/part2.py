import argparse
import collections

Target = collections.namedtuple("Target", ["id", "point"])
PointCost = collections.namedtuple("PointCost", ["id", "cost"])


def manhattan_distance(point1, point2):
    return abs(point2.real - point1.real) + abs(point2.imag - point1.imag)


def load_targets(fp):
    targets = []
    for index, line in enumerate(fp):
        coords = [int(num) for num in line.strip().split(", ")]
        target = Target(index, complex(*coords))
        targets.append(target)
    return targets


parser = argparse.ArgumentParser()
parser.add_argument("filename")
parser.add_argument("limit", type=int)
args = parser.parse_args()

with open(args.filename) as fp:
    targets = load_targets(fp)

x_min = int(min(t.point.real for t in targets))
x_max = int(max(t.point.real for t in targets))
y_min = int(min(t.point.imag for t in targets))
y_max = int(max(t.point.imag for t in targets))

graph = collections.Counter()
for y in range(y_min, y_max+1):
    for x in range(x_min, x_max+1):
        for target in targets:
            point = complex(x, y)
            cost = manhattan_distance(point, target.point)
            graph[point] += cost

area = 0
for cost in graph.values():
    if cost < args.limit:
        area += 1

print(area)
