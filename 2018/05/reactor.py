import reactor

def are_opposites(unit1, unit2):
    return unit1 == unit2.swapcase()

def find_opposites(polymer, start):
    for i in range(start+1, len(polymer)):
        if are_opposites(polymer[i-1], polymer[i]):
            return i-1
    return -1

def fully_react(polymer):
    pos = 0
    while True:
        pos = find_opposites(polymer, max(pos-1, 0))
        if pos < 0:
            return polymer
        polymer = polymer[:pos] + polymer[pos+2:]
