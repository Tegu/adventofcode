import argparse

import reactor

parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

with open(args.filename) as fp:
    polymer = fp.read().strip()
    reacted_polymer = reactor.fully_react(polymer)
    print(len(reacted_polymer))
