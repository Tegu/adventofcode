import argparse

import reactor

def remove_unit(polymer, unit):
    return polymer.replace(unit, "").replace(unit.swapcase(), "")

parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

with open(args.filename) as fp:
    polymer = fp.read().strip()

lengths = {}
for i in range(ord("a"), ord("z")):
    char = chr(i)
    filtered_polymer = remove_unit(polymer, char)
    reacted_polymer = reactor.fully_react(filtered_polymer)
    lengths[char] = len(reacted_polymer)

min_char, min_length = min(lengths.items(), key=lambda x: x[1])
print(min_char, min_length)
