fn main() {
    let target_recipe_count = 846021;
    //let target_recipe_count = 2018;
    let target_scores = [8, 4, 6, 0, 2, 1];
    //let target_scores = [5, 9, 4, 1, 4];
    let mut current = [0, 1];
    let mut scores = vec![3, 7];
    let mut finished = false;
    let mut recipe_offset = 0;
    let mut last_offset = 0;

    while !finished {
        let score_sum = scores[current[0]] + scores[current[1]];
        let digit_0 = score_sum % 10;
        let digit_1 = (score_sum / 10) % 10;
        //println!("Current: {}(value {}), {}(value {})", current[0], scores[current[0]], current[1], scores[current[1]]);
        //println!("Sum: {}. Digits {}, {}", score_sum, digit_1, digit_0);
        //print_scores(&scores);
        if score_sum > 9 {
            scores.push(digit_1);
        }
        scores.push(digit_0);

        let scores_len = scores.len();
        for i in 0..current.len() {
            current[i] = (current[i] + 1 + scores[current[i]]) % scores_len;
        }

        if scores_len >= target_scores.len() {
            for offset in last_offset..scores_len-target_scores.len() {
                let mut found = true;
                for i in 0..target_scores.len() {
                    if scores[offset + i] != target_scores[i] {
                        found = false;
                        break;
                    }
                }
                if found {
                    finished = true;
                    break;
                }
                recipe_offset = offset;
            }
            last_offset = scores_len - target_scores.len();
        }
    }
    println!("Recipes on the left side: {}", recipe_offset + 1);
}

fn print_scores(scores: &Vec<usize>) {
    for i in 0..scores.len() {
        print!("{}", scores[i]);
    }
    print!("\n");
}

fn print_last_scores(scores: &Vec<usize>, start: usize, count: usize) {
    for i in start..start+count {
        print!("{}", scores[i]);
    }
    print!("\n");
}
