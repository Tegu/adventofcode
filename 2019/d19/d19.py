import asyncio
import sys
import time

from d09 import d09

SIZE = 100


def print_world(world):
    left = min(world.keys(), key=lambda p: p[0])[0]
    right = max(world.keys(), key=lambda p: p[0])[0]
    top = min(world.keys(), key=lambda p: p[1])[1]
    bottom = max(world.keys(), key=lambda p: p[1])[1]
    print('Extents', left, top, right, bottom)
    for y in range(top, bottom+1):
        for x in range(left, right+1):
            status = world.get((x, y), None)
            char = ' '
            if status == 0:
                char = '.'
            elif status == 1:
                char = '#'
            print(char, end='')
        print()


async def main():
    program = d09.lexer(sys.stdin.readline()) + [0]*100

    world = {}
    beam = {}
    top = 0
    bottom = 5
    for x in range(1200):
        beam_top = None
        beam_bottom = None
        for y in range(top, bottom + 1):
            queue_in = asyncio.Queue()
            queue_out = asyncio.Queue()
            task = asyncio.ensure_future(d09.eval_program(list(program), queue_in, queue_out))
            await queue_in.put(x)
            await queue_in.put(y)
            status = await queue_out.get()
            if status == 1 and beam_top is None:
                beam_top = y
            if status == 0 and beam_top is not None:
                beam_bottom = y - 1
                break
            world[x, y] = int(status)
            task.cancel()
        if beam_top is not None:
            top = beam_top
            bottom = beam_bottom + 5
            beam[x] = (beam_top, beam_bottom)
        left_x = x - (SIZE - 1)
        if beam.get(left_x, None):
            delta = beam[left_x][1] - beam_top
            if x % 10 == 0:
                print(x, delta)
            if delta + 1 >= SIZE:
                print('Sufficient space found! left beam x {}, y {}; right beam x {}, y {}'.format(left_x, beam[left_x], x, beam[x]))
                print('Part 2: ', 10000 * left_x + beam_top)
                break

    #print_world(world)
    count = sum(world.values())
    print('Pull point count:', count)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

# 9880491 too high
# 9820488 too high
