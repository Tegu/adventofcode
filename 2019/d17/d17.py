import subprocess
import sys


def print_world(world):
    width = max(world.keys(), key=lambda p: p[0])[0]
    height = max(world.keys(), key=lambda p: p[1])[1]
    print(end='  ')
    for x in range(width):
        print(x // 10, end='')
    print()
    print(end='  ')
    for x in range(width):
        print(x % 10, end='')
    print()
    for y in range(height):
        print('{:02d}'.format(y), end='')
        for x in range(width):
            print(world[(x, y)], end='')
        print()


proc = subprocess.Popen('python3 -u ../d09/d09.py input.txt'.split(),
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE
        )


world = dict()

x = 0
y = 0
while True:
    char = chr(int(proc.stdout.readline().strip()))
    if x == 0 and char == '\n':
        break
    elif char == '\n':
        x = 0
        y += 1
    else:
        world[(x, y)] = char
        x += 1

print_world(world)
