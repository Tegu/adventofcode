import collections
import math
import sys

OutputNeeds = collections.namedtuple('OutputNeeds', ('count', 'needs'))


def parse_counts(side):
    for pair in side.split(', '):
        count, chemical = pair.split()
        yield chemical, int(count)


def calc_ore_for_fuel(reactions, fuel_wanted):
    ore_count = 0

    storage = collections.defaultdict(int)

    fuel_needs = collections.defaultdict(int)
    fuel_needs['FUEL'] = fuel_wanted

    while len(fuel_needs) > 0:
        chemical, needed_count = fuel_needs.popitem()
        if storage[chemical] > 0:
            if needed_count >= storage[chemical]:
                needed_count -= storage[chemical]
                storage[chemical] = 0
            else:
                storage[chemical] -= needed_count
                needed_count = 0
                continue
        if chemical == 'ORE':
            ore_count += needed_count
            continue
        produced_count = reactions[chemical].count
        multiplier = int(math.ceil(needed_count / produced_count))
        storage[chemical] += produced_count * multiplier - needed_count
        for subchemical, count in reactions[chemical].needs.items():
            fuel_needs[subchemical] += count * multiplier

    return ore_count



reactions = {}

for line in sys.stdin:
    need_side, output_side = line.strip().split(' => ')
    needs = dict(parse_counts(need_side))
    output = tuple(parse_counts(output_side))[0]
    reactions[output[0]] = OutputNeeds(output[1], needs)

ore_needed = calc_ore_for_fuel(reactions, 1)
print('Part 1: Ore needed for 1 FUEL unit:', ore_needed)


usable_ore = 1000000000000


# Exponential search (?)
fuel_amount = 1
min_amount = fuel_amount
max_amount = fuel_amount

while True:
    ore_needed = calc_ore_for_fuel(reactions, fuel_amount)
    if ore_needed < usable_ore:
        min_amount = fuel_amount
        fuel_amount *= 10
    else:
        max_amount = fuel_amount
        break


# Binary search
while True:
    if min_amount == max_amount - 1:
        break
    fuel_amount = (max_amount - min_amount) // 2 + min_amount
    ore_needed = calc_ore_for_fuel(reactions, fuel_amount)
    if ore_needed < usable_ore:
        min_amount = fuel_amount
    else:
        max_amount = fuel_amount

print('Part 2: Maximum producable fuel with {} ore: {}'.format(usable_ore, min_amount))
