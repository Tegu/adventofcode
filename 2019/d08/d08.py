import argparse
import collections
import functools
import sys

Image = collections.namedtuple('Image', ['width', 'height', 'layers'])

def dprint(*args, **kwargs):
    return print(*args, **kwargs, file=sys.stderr)

def parse_layer(section, width, height):
    layer = []
    for y in range(height):
        row = list(map(int, section[y*width : (y+1)*width]))
        layer.append(row)
    return layer

def decode_image(line, width, height):
    layer_size = width * height
    assert line.isnumeric()
    assert len(line) % layer_size == 0
    layers = []
    for i in range(len(line) // layer_size):
        section = line[i*layer_size : (i+1)*layer_size]
        layer = parse_layer(section, width, height)
        layers.append(layer)
    return Image(width, height, layers)

def count_layer_digits(layer, digit):
    total = 0
    for row in layer:
        total += sum(map(lambda d: d == digit, row))
    return total

def flatten_image(image):
    flat_layer = [[2]*image.width for _ in range(image.height)]
    for y in range(image.height):
        for x in range(image.width):
            for layer in image.layers:
                if layer[y][x] != 2:
                    flat_layer[y][x] = layer[y][x]
                    break
    return Image(image.width, image.height, [flat_layer])

def print_layer(layer):
    for row in layer:
        print(''.join(str(d) for d in row))



parser = argparse.ArgumentParser()
parser.add_argument('width', type=int)
parser.add_argument('height', type=int)
parser.add_argument('filename', nargs='?')
args = parser.parse_args()

if args.filename:
    line = open(args.filename).readline().strip()
else:
    line = sys.stdin.readline().strip()

image = decode_image(line, args.width, args.height)
zero_layer = min(image.layers, key=functools.partial(count_layer_digits, digit=0))
dprint('Fewest zeros:', zero_layer)
result = count_layer_digits(zero_layer, 1) * count_layer_digits(zero_layer, 2)
print('Number of 1 digits multiplied by 2 digits:', result)

flat_image = flatten_image(image)
print_layer(flat_image.layers[0])
