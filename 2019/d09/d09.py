import asyncio
import sys

def dprint(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)

def lexer(text):
    return list(map(int, text.strip().split(',')))

def split_digits(num, n):
    digits = [0]*n
    for i in range(n):
        digits[i] = num % 10
        num = num // 10
    return tuple(digits)

def parse_op_modes(digits):
    opcode = digits[0] + 10 * digits[1]
    modes = digits[2:]
    return opcode, modes

def resolve_parameters(program, params, modes, relative_base):
    assert(len(params) <= len(modes))
    values = [0]*len(params)
    for i, param in enumerate(params):
        if modes[i] == 0:
            values[i] = program[param]
        elif modes[i] == 1:
            values[i] = param
        elif modes[i] == 2:
            values[i] = program[relative_base + param]
    return values

async def eval_program(program, queue_in, queue_out, event_in, pc=0, relative_base=0):
    while True:
        value = program[pc]
        opcode, modes = parse_op_modes(split_digits(value, 5))
        if opcode == 99:
            return
        elif opcode == 1:
            assert(modes[2] != 1)
            params = program[pc+1:pc+4]
            values = resolve_parameters(program, params, modes, relative_base)
            output_location = params[2] + (0 if modes[-1] != 2 else relative_base)
            program[output_location] = values[0] + values[1]
            pc += 4
        elif opcode == 2:
            assert(modes[2] != 1)
            params = program[pc+1:pc+4]
            values = resolve_parameters(program, params, modes, relative_base)
            output_location = params[2] + (0 if modes[-1] != 2 else relative_base)
            program[output_location] = values[0] * values[1]
            pc += 4
        elif opcode == 3:
            event_in.set()
            input_value = await queue_in.get()
            output_location = program[pc+1] + (0 if modes[0] != 2 else relative_base)
            program[output_location] = input_value
            pc += 2
        elif opcode == 4:
            params = [program[pc+1]]
            values = resolve_parameters(program, params, modes, relative_base)
            await queue_out.put(values[0])
            pc += 2
        elif opcode == 5:
            params = program[pc+1:pc+3]
            values = resolve_parameters(program, params, modes, relative_base)
            if values[0] != 0:
                pc = values[1]
            else:
                pc += 3
        elif opcode == 6:
            params = program[pc+1:pc+3]
            values = resolve_parameters(program, params, modes, relative_base)
            if values[0] == 0:
                pc = values[1]
            else:
                pc += 3
        elif opcode == 7:
            params = program[pc+1:pc+4]
            values = resolve_parameters(program, params, modes, relative_base)
            output_location = params[2] + (0 if modes[-1] != 2 else relative_base)
            program[output_location] = int(values[0] < values[1])
            pc += 4
        elif opcode == 8:
            params = program[pc+1:pc+4]
            values = resolve_parameters(program, params, modes, relative_base)
            output_location = params[2] + (0 if modes[-1] != 2 else relative_base)
            program[output_location] = int(values[0] == values[1])
            pc += 4
        elif opcode == 9:
            params = [program[pc+1]]
            values = resolve_parameters(program, params, modes, relative_base)
            relative_base += values[0]
            pc += 2
        else:
            print('Encountered unknown opcode {} at {}'.format(opcode, pc), file=sys.stderr)
            return


async def put_ascii(queue: asyncio.Queue, event_in: asyncio.Event, text: str):
    for char in text:
        await event_in.wait()
        await queue.put(ord(char))
        event_in.clear()


async def get_line(queue: asyncio.Queue):
    chars = []
    while True:
        num = await queue.get()
        if num >= 128:
            # Outside of ASCII range
            return ''.join(chars), num
        char = chr(num)
        chars.append(char)
        if char == '\n':
            break
    return ''.join(chars), None


async def stdout_consumer(queue_out):
    while True:
        line = await queue_out.get()
        sys.stdout.write(chr(line))
        sys.stdout.flush()


async def stdin_consumer(queue_in, event_in):
    while True:
        await event_in.wait()
        line = sys.stdin.readline()
        await put_ascii(queue_in, event_in, line)
        event_in.clear()


def main():
    if len(sys.argv) == 1:
        program = lexer(sys.stdin.readline())
    if len(sys.argv) == 2:
        program = lexer(open(sys.argv[1]).read())
    else:
        print('Program not found', file=sys.stderr)
        sys.exit(1)
    queue_in = asyncio.Queue(maxsize=1)
    queue_out = asyncio.Queue(maxsize=1)
    event_in = asyncio.Event()
    loop = asyncio.get_event_loop()
    loop.create_task(stdout_consumer(queue_out))
    loop.create_task(stdin_consumer(queue_in, event_in))
    loop.run_until_complete(eval_program(program + [0]*10000, queue_in, queue_out, event_in))


if __name__ == '__main__':
    main()
