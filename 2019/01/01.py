import sys

def calculate_fuel(mass):
    total = 0
    required = mass
    while True:
        required = max(0, required // 3 - 2)
        total += required
        if required == 0:
            break
    return total

def parse_mass(line):
    return int(line.strip())

masses = map(parse_mass, sys.stdin)
fuels = map(calculate_fuel, masses)
total = sum(fuels)
print('Sum of fuel: {}'.format(total))
