#!/bin/bash
# HACKY! :x
amp='python3 -u ../05/05.py input.txt'
rm -f /tmp/myfifo
mkfifo /tmp/myfifo
for ar in `python3 -c 'import itertools; any(print("".join(s)) for s in itertools.permutations("56789"))'`
do
    (echo "${ar:4:1}";
        (echo "${ar:3:1}";
            (echo "${ar:2:1}";
                (echo "${ar:1:1}";
                    (echo "${ar:0:1}"; echo 0; cat /tmp/myfifo) | $amp) \
                | $amp) \
            | $amp) \
        | $amp) \
    | $amp \
    | tee "part2/${ar}.txt" > /tmp/myfifo
done
# result gathering
(find part2 -type f -exec tail -n1 {} \;) | sort -nr | head -n1
