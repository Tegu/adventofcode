import collections
import sys

def is_increasing(password):
    prev = password[0]
    for char in password[1:]:
        if prev > char:
            return False
        prev = char
    return True

def test_password(password):
    if not is_increasing(password):
        return False
    counts = collections.Counter(password)
    return any(map(lambda x: x == 2, counts.values()))

assert(test_password('111111') == False)
assert(test_password('223450') == False)
assert(test_password('123789') == False)
assert(test_password('112233') == True)
assert(test_password('123444') == False)
assert(test_password('111122') == True)
assert(test_password('112333') == True)
assert(test_password('112223') == True)
assert(test_password('123456') == False)

password_range = sys.stdin.read().strip()
lower, upper = map(int, password_range.split('-'))

count = 0
for num in range(lower, upper+1):
    if test_password(str(num)):
        count += 1

print('Passwords matching criteria:', count)
