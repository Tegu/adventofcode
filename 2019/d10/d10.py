import collections
import math
import sys

Vector = collections.namedtuple('Vector', ['x', 'y'])
Result = collections.namedtuple('Result', ['pos', 'visible'])



def gcd(a, b):
    if a == 0 or b == 0:
        raise ValueError('neither can be zero')
    x = max(abs(a), abs(b))
    y = min(abs(a), abs(b))
    while True:
        q = x // y
        r = x % y
        if r == 0:
            break
        x = y
        y = r
    return y


def integer_multiplier(vec):
    if vec.x == 0 or vec.y == 0:
        return max(abs(vec.x), abs(vec.y))
    return gcd(vec.x, vec.y)


def integer_normalized(vec):
    r = integer_multiplier(vec)
    assert r != 0  # no zero vectors allowed
    return (Vector(vec.x // r, vec.y // r), r)


def asteroid_los(asteroids, asteroid1, asteroid2):
    assert(asteroid1 != asteroid2)
    delta = Vector(
            asteroid2.x-asteroid1.x,
            asteroid2.y-asteroid1.y,
            )
    norm, r = integer_normalized(delta)
    for i in range(1, r):
        target = Vector(
                asteroid1.x + i * norm.x,
                asteroid1.y + i * norm.y
                )
        if target in asteroids:
            return False
    return True


def parse_asteroids(map_text):
    asteroids = set([])
    for y, line in enumerate(map_text.split('\n')):
        for x, cell in enumerate(line):
            if cell == '#':
                asteroids.add(Vector(x, y))
    return asteroids


def asteroid_finder(asteroids):
    seen = collections.defaultdict(set)
    for asteroid1 in asteroids:
        for asteroid2 in asteroids:
            if asteroid1 == asteroid2:
                continue
            if asteroid_los(asteroids, asteroid1, asteroid2):
                seen[asteroid1].add(asteroid2)
    results = (Result(a, s) for a, s in seen.items())
    return max(results, key=lambda r: len(r.visible))


def asteroid_angles(asteroids, reference):
    for asteroid in asteroids:
        angle = math.atan2(
                asteroid.y - reference.y,
                asteroid.x - reference.x,
                )
        yield asteroid, angle

if __name__ == '__main__':
    assert gcd(1, 1) == 1
    assert gcd(3, 2) == 1
    assert gcd(2, 3) == 1
    assert gcd(4, 2) == 2
    assert gcd(2, 4) == 2
    assert gcd(-2, 4) == 2
    assert gcd(2, -4) == 2

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs='?')
    args = parser.parse_args()

    if args.filename:
        with open(args.filename) as fp:
            asteroids = parse_asteroids(fp.read())
    else:
        asteroids = parse_asteroids(sys.stdin.read())

    result = asteroid_finder(asteroids)
    print('Part 1: Best asteroid at ({0.x}, {0.y}) with {1} other asteroids visible'.format(result.pos, len(result.visible)))
    print('visible', list(sorted(result.visible)))
    assert len(result.visible) >= 200  # lower count needs a better solution
    transform_angles = lambda x: (x[0], (x[1] - 3*math.pi/2) % (2 * math.pi))
    offset_targets = map(transform_angles, asteroid_angles(result.visible, result.pos))
    sorted_targets = sorted(offset_targets, key=lambda x: x[1])
    for i, target in enumerate(sorted_targets):
        print(i+1, target, '!!!!' if i+1 == 200 else '')

    # 1622 was too high
    # 206 was too low
