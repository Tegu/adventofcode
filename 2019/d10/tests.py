import unittest

import d10

class Test1Norm(unittest.TestCase):

    def test_normalizing(self):
        self.assertEqual(d10.integer_normalized(
            d10.Vector(2, 2)),
            (d10.Vector(1, 1), 2))
        self.assertEqual(d10.integer_normalized(
            d10.Vector(2, -7)),
            (d10.Vector(2, -7), 1))
        self.assertEqual(d10.integer_normalized(
            d10.Vector(2, 0)),
            (d10.Vector(1, 0), 2))
        self.assertEqual(d10.integer_normalized(
            d10.Vector(-2, 0)),
            (d10.Vector(-1, 0), 2))



class Test2LOS(unittest.TestCase):

    def test_simple(self):
        asteroids = [
            d10.Vector(0 - 4, 0 + 3),
            d10.Vector(1 - 4, 1 + 3),
            d10.Vector(2 - 4, 2 + 3),
            ]
        self.assertTrue(d10.asteroid_los(
            asteroids,
            asteroids[0],
            asteroids[1],
            ))
        self.assertTrue(d10.asteroid_los(
            asteroids,
            asteroids[1],
            asteroids[0],
            ))
        self.assertFalse(d10.asteroid_los(
            asteroids,
            asteroids[0],
            asteroids[2],
            ))

    def test_horizontal(self):
        asteroids = [
            d10.Vector(1, 6),
            d10.Vector(3, 6),
            d10.Vector(10, 6),
            ]
        self.assertTrue(d10.asteroid_los(
            asteroids,
            asteroids[0],
            asteroids[1],
            ))
        self.assertTrue(d10.asteroid_los(
            asteroids,
            asteroids[1],
            asteroids[2],
            ))
        self.assertFalse(d10.asteroid_los(
            asteroids,
            asteroids[0],
            asteroids[2],
            ))

    def test_vertical(self):
        asteroids = [
            d10.Vector(6, 1),
            d10.Vector(6, 3),
            d10.Vector(6, 10),
            ]
        self.assertTrue(d10.asteroid_los(
            asteroids,
            asteroids[0],
            asteroids[1],
            ))
        self.assertTrue(d10.asteroid_los(
            asteroids,
            asteroids[1],
            asteroids[2],
            ))
        self.assertFalse(d10.asteroid_los(
            asteroids,
            asteroids[0],
            asteroids[2],
            ))





class TestExamples(unittest.TestCase):

    def test_example1(self):
        result = d10.asteroid_finder(d10.parse_asteroids(
                '.#..#\n'
                '.....\n'
                '#####\n'
                '....#\n'
                '...##\n'
                ))
        self.assertEqual(result.pos, (3, 4))
        self.assertEqual(len(result.visible), 8)

    def test_example2(self):
        result = d10.asteroid_finder(d10.parse_asteroids(
                '......#.#.\n'
                '#..#.#....\n'
                '..#######.\n'
                '.#.#.###..\n'
                '.#..#.....\n'
                '..#....#.#\n'
                '#..#....#.\n'
                '.##.#..###\n'
                '##...#..#.\n'
                '.#....####\n'
                ))
        self.assertEqual(result.pos, (5, 8))
        self.assertEqual(len(result.visible), 33)

    def test_example3(self):
        result = d10.asteroid_finder(d10.parse_asteroids(
                '#.#...#.#.\n'
                '.###....#.\n'
                '.#....#...\n'
                '##.#.#.#.#\n'
                '....#.#.#.\n'
                '.##..###.#\n'
                '..#...##..\n'
                '..##....##\n'
                '......#...\n'
                '.####.###.\n'
                ))
        self.assertEqual(result.pos, (1, 2))
        self.assertEqual(len(result.visible), 35)

    def test_example4(self):
        result = d10.asteroid_finder(d10.parse_asteroids(
                '.#..#..###\n'
                '####.###.#\n'
                '....###.#.\n'
                '..###.##.#\n'
                '##.##.#.#.\n'
                '....###..#\n'
                '..#.#..#.#\n'
                '#..#.#.###\n'
                '.##...##.#\n'
                '.....#.#..\n'
                ))
        self.assertEqual(result.pos, (6, 3))
        self.assertEqual(len(result.visible), 41)

    def test_example5(self):
        result = d10.asteroid_finder(d10.parse_asteroids(
                '.#..##.###...#######\n'
                '##.############..##.\n'
                '.#.######.########.#\n'
                '.###.#######.####.#.\n'
                '#####.##.#.##.###.##\n'
                '..#####..#.#########\n'
                '####################\n'
                '#.####....###.#.#.##\n'
                '##.#################\n'
                '#####.##.###..####..\n'
                '..######..##.#######\n'
                '####.##.####...##..#\n'
                '.#####..#.######.###\n'
                '##...#.##########...\n'
                '#.##########.#######\n'
                '.####.#.###.###.#.##\n'
                '....##.##.###..#####\n'
                '.#.#.###########.###\n'
                '#.#.#.#####.####.###\n'
                '###.##.####.##.#..##\n'
                ))
        self.assertEqual(result.pos, (11, 13))
        self.assertEqual(len(result.visible), 210)


if __name__ == '__main__':
    unittest.main()
