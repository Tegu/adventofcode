import argparse
import collections

parser = argparse.ArgumentParser()
parser.add_argument('filename')
parser.add_argument('card_count', type=int, nargs='?', default=10007)
parser.add_argument('target_card', type=int, nargs='?', default=2019)
args = parser.parse_args()

deck = collections.deque(range(args.card_count))

with open(args.filename) as fp:
    table = [None] * len(deck)
    for line in fp:
        if line.startswith('deal into new stack'):
            deck.reverse()
        elif line.startswith('deal with increment'):
            increment = int(line.rstrip().rsplit(maxsplit=1)[-1])
            for i in range(len(deck)):
                table[i*increment % len(table)] = deck.popleft()
            deck.extend(table)
        elif line.startswith('cut'):
            n = int(line.rstrip().rsplit(maxsplit=1)[-1])
            deck.rotate(-n)

# Find the target card
for i, card in enumerate(deck):
    if card == args.target_card:
        print('Card #{} found at position {}'.format(args.target_card, i))
        break
