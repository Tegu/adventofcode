import subprocess
import sys

def eprint(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)

def eprint_screen(screen):
    for y in range(24):
        for x in range(50):
            eprint(screen.get((x, y), ' '), end='')
        eprint()

proc = subprocess.Popen('python3 -u ../d09/d09.py input_freeplay.txt'.split(),
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE)

screen = dict()

prev_ball = (0, 0)
ball_pos = (0, 0)
paddle_pos = (0, 0)

score = -1337
while True:
    x = int(proc.stdout.readline().strip())
    y = int(proc.stdout.readline().strip())
    num = int(proc.stdout.readline().strip())
    eprint(x, y, num)
    if num == 3:
        paddle_pos = (x, y)
    elif num == 4:
        prev_ball = ball_pos
        ball_pos = (x, y)
        if prev_ball == (0, 0):
            prev_ball = ball_pos
    if x == -1 and y == 0:
        score = num
        eprint('Score', score)
    else:
        screen[(x, y)] = num
    if num == 3:
        #eprint_screen(screen)
        pass
    if (x == 43 and y == 23) or (num == 4 and ball_pos[1] < paddle_pos[1]):
        #eprint_screen(screen)
        ball_delta = ball_pos[0] - prev_ball[0], ball_pos[1] - prev_ball[1]
        target_x = ball_pos[0] + ball_delta[0]
        if (screen.get((ball_pos[0] + ball_delta[0], ball_pos[1]), 0) == 2
                or screen.get((ball_pos[0] + ball_delta[0], ball_pos[1] + ball_delta[1]), 0) == 2):
            target_x = ball_pos[0]
        #target_x = 23
        eprint('Paddle {}, Ball {}, Ball delta {}, Target x {}'.format(paddle_pos, ball_pos, ball_delta, target_x))
        delta = min(1, max(-1, target_x - paddle_pos[0]))
        #delta = int(sys.stdin.readline().strip())
        eprint('Moving paddle by {}'.format(delta))
        proc.stdin.write('{}\n'.format(delta).encode())
        proc.stdin.flush()
eprint('Final Score', score)
