import functools
import math
import sys

import numpy


def parse_positions(text):
    positions = []
    lines = text.split('\n')
    for line in lines:
        fields = line.strip('<>').split(', ')
        if len(fields) != 3:
            continue
        coordinates = [int(field.split('=')[1]) for field in fields]
        positions.append(coordinates)
    return numpy.array(positions)


def calc_velocity(position, positions):
    return (positions - position).clip(-1, 1).sum(axis=0)


positions = parse_positions(sys.stdin.read())
velocities = numpy.zeros_like(positions)

N = 500000

all_velocities = numpy.ones(velocities.shape + (N, ), velocities.dtype)

for step in range(N):
    for moon_i in range(velocities.shape[0]):
        for dim_i in range(velocities.shape[1]):
            all_velocities[moon_i, dim_i, step] = velocities[moon_i, dim_i]

    delta_velocity = numpy.apply_along_axis(calc_velocity, 1, positions, positions)
    velocities += delta_velocity
    positions += velocities

    #total_energies = numpy.absolute(positions).sum(axis=1) \
            #* numpy.absolute(velocities).sum(axis=1)

print('After {} steps'.format(step + 1))
print('Positions')
print(positions)
print('Velocities')
print(velocities)
#print('Total energy after step #{} is {}'.format(step, total_energies.sum()))

def find_period_length(a):
    for i in range(1, len(a)):
        for j in range(i):
            if i+j >= len(a) or a[j] != a[i+j]:
                break
        else:
            return j + 1
    return 0  # no period found.. or what

print('Velocity periods')
period_lengths = []
for moon_i in range(all_velocities.shape[0]):
    for dim_i in range(all_velocities.shape[1]):
        length = find_period_length(all_velocities[moon_i, dim_i])
        print(moon_i, dim_i, length)
        period_lengths.append(length)


total_period = functools.reduce(lambda acc, y: acc * y // math.gcd(acc, y), sorted(period_lengths, reverse=True))
print('Part 2: First repeating state after {} steps'.format(total_period))
