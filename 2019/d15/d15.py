import collections
import enum
import random
import subprocess
import sys


class Tile(enum.IntEnum):
    Unknown = 0
    Floor = 1
    Wall = 2
    Oxygen = 3


class Status(enum.IntEnum):
    HitWall = 0
    Moved = 1
    MovedToOxygen = 2


class Movement(enum.IntEnum):
    North = 1
    South = 2
    West = 3
    East = 4


def move(position, movement):
    x, y = position
    if movement == Movement.North:
        return x, y - 1
    elif movement == Movement.South:
        return x, y + 1
    elif movement == Movement.East:
        return x + 1, y
    elif movement == Movement.West:
        return x - 1, y
    else:
        return ValueError('Invalid movement {}'.format(movement))


def get_opposite(movement):
    if movement == Movement.North:
        return Movement.South
    elif movement == Movement.South:
        return Movement.North
    elif movement == Movement.East:
        return Movement.West
    elif movement == Movement.West:
        return Movement.East


def get_neighbors(position):
    return dict((movement, move(position, movement)) for movement in Movement)


def print_world(world, pos):
    left = min(world.keys(), key=lambda p: p[0])[0]
    right = max(world.keys(), key=lambda p: p[0])[0]
    top = min(world.keys(), key=lambda p: p[1])[1]
    bottom = max(world.keys(), key=lambda p: p[1])[1]
    print('Extents', left, top, right, bottom)
    chars = ' .#O'
    assert len(chars) == len(Tile)
    for y in range(top, bottom+1):
        for x in range(left, right+1):
            tile = world.get((x, y), Tile.Unknown)
            char = chars[tile.value]
            if (x, y) == pos:
                char = '@'
            elif (x, y) == (0, 0):
                char = 'X'
            print(char, end='')
        print()


def trace_parents(parent, start):
    path = []
    pos = start
    while True:
        prev = parent.get(pos, None)
        if prev is None:
            break
        path.append(prev)
        pos = prev
    return path


def bfs(world, start):
    queue = collections.deque()
    discovered = set()
    parent = dict()
    distance = dict()
    discovered.add(start)
    queue.append(start)
    while len(queue) > 0:
        pos = queue.popleft()
        for neighbor in get_neighbors(pos).values():
            if world[neighbor] == Tile.Wall:
                continue
            if neighbor not in discovered:
                discovered.add(neighbor)
                parent[neighbor] = pos
                distance[neighbor] = distance.get(pos, 0) + 1
                queue.append(neighbor)
    max_range = max(distance.values())
    print('Max range', max_range)



def print_enum(enum_, ofile):
    ofile.write('{}\n'.format(enum_.value).encode())
    ofile.flush()


def read_enum(enum_type, ifile):
    return enum_type(int(ifile.readline()))



proc = subprocess.Popen('python3 -u ../d09/d09.py input.txt'.split(),
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE)

world = dict()
pos = (0, 0)
world[pos] = Tile.Floor

movement_log = []  # tracks successful moves only

for step_i in range(100000):
    backtrack = False
    neighbors = get_neighbors(pos)
    unknowns = tuple(m for m, p in neighbors.items() if world.get(p, Tile.Unknown) == Tile.Unknown)
    if len(unknowns) > 0:
        movement = unknowns[0]
    elif len(movement_log) > 0:
        backtrack = True
        movement = get_opposite(movement_log.pop())
    else:
        print('Empty movement log, RANDOM', file=sys.stderr)
        #movement = random.choice(tuple(Movement))
        break
    print_enum(movement, proc.stdin)
    status = read_enum(Status, proc.stdout)
    #print('Status', status)
    new_pos = move(pos, movement)
    if status == Status.HitWall:
        world[new_pos] = Tile.Wall
    elif status == Status.Moved:
        world[new_pos] = Tile.Floor
        pos = new_pos
        if not backtrack:
            movement_log.append(movement)
    elif status == Status.MovedToOxygen:
        world[new_pos] = Tile.Oxygen
        pos = new_pos
        print('OXYGEN found at {}'.format(new_pos))
        print(len(movement_log), movement_log)
        if not backtrack:
            movement_log.append(movement)

print_world(world, pos)

oxygen_pos = tuple(pos for pos, tile in world.items() if tile == Tile.Oxygen)
assert len(oxygen_pos) == 1

bfs(world, oxygen_pos[0])

# oxygen found at (14, -14)
# 231 moves too low
