import collections
import sys

Move = collections.namedtuple('Move', ['direction', 'amount'])

def manhattan(c1, c2):
    diff = c2 - c1
    return abs(diff.real) + abs(diff.imag)

def parse_move(move):
    assert(len(move) >= 2)
    direction = move[0]
    amount = int(move[1:])
    direction_vectors = {
            'U': complex(0, 1),
            'R': complex(1, 0),
            'D': complex(0, -1),
            'L': complex(-1, 0),
            }
    vector = direction_vectors[direction]
    return Move(vector, amount)

def parse_wire(text):
    moves = [parse_move(move.strip()) for move in text.split(',')]
    return moves

grid = collections.defaultdict(set)

wires = [parse_wire(line.strip()) for line in sys.stdin if line.strip()]

for wire_num, wire in enumerate(wires):
    current = complex(0, 0)
    for move in wire:
        for i in range(move.amount):
            grid[current].add(wire_num)
            current += move.direction

intersections = [point for point, wire_nums in grid.items() if len(wire_nums) == 2 and point != 0j]
print('Intersections', intersections)

closest_intersection = min(intersections, key=lambda p: manhattan(p, 0j))
closest_distance = int(manhattan(closest_intersection, 0j))
print('Manhattan distance to ({0.real}, {0.imag}) is {1}'.format(closest_intersection, closest_distance))

delays = collections.defaultdict(int)
for wire_num, wire in enumerate(wires):
    delay = 0
    current = complex(0, 0)
    for move in wire:
        for _ in range(move.amount):
            current += move.direction
            delay += 1
            if current in intersections:
                delays[current] += delay

print(delays)
lowest_delay = min(delays.items(), key=lambda item: item[1])
print('Lowest delay:', lowest_delay)
