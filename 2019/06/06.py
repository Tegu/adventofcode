import collections
import sys

class OrbitObject:
    def __init__(self):
        self.parent = None
        self.children = []
    def __str__(self):
        return '[{}){}]'.format(self.parent, self.children)
    def __repr__(self):
        return 'OrbitObject[{}){}]'.format(self.parent, self.children)

def parse_objects(orbit_pairs):
    objects = collections.defaultdict(OrbitObject)
    for pair in orbit_pairs:
        assert len(pair) == 2
        parent, child = pair
        objects[parent].children.append(child)
        objects[child].parent = parent
    return objects

def dfs_depths(objects, depths, parent):
    depth = depths[parent]
    children = objects[parent].children
    for child in children:
        depths[child] = depth + 1
        dfs_depths(objects, depths, child)

def resolve_depths(objects):
    assert 'COM' in objects
    depths = {}
    depths['COM'] = 0
    dfs_depths(objects, depths, 'COM')
    return depths

def ancestors(objects, name):
    path = []
    current = name
    while True:
        parent = objects[current].parent
        if parent is None:
            break
        path.append(parent)
        current = parent
    return path

lines = [line.strip().split(')') for line in sys.stdin.readlines()]
objects = parse_objects(lines)
#print(objects, file=sys.stderr)
depths = resolve_depths(objects)
count = sum(depths.values())
print('Number of all orbits:', count)

#print('SAN', objects['SAN'], ancestors(objects, 'SAN'), file=sys.stderr)
#print('YOU', objects['YOU'], ancestors(objects, 'YOU'), file=sys.stderr)

santa_ancestors = set(ancestors(objects, 'SAN'))
you_ancestors = set(ancestors(objects, 'YOU'))

incommon_ancestors = santa_ancestors ^ you_ancestors

print('Incommon ancestors: {} (count: {})'.format(incommon_ancestors, len(incommon_ancestors)))
