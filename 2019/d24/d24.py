import argparse
import copy
import itertools
import typing


BUG = '#'
EMPTY = '.'


def print_field(field: typing.List[typing.List[str]]):
    for y in range(1, len(field) - 1):
        for x in range(1, len(field) - 1):
            print(field[y][x], end='')
        print()


def calc_biodiversity(field: typing.List[typing.List[str]]):
    ranking = 0
    N = len(field) - 2
    for y in range(N):
        for x in range(N):
            if field[y+1][x+1] == BUG:
                ranking += 2**(y*N + x)
    return ranking


def get_adjacent(field: typing.List[typing.List[str]], x: int, y: int):
    assert 0 < x < len(field) - 1 and 0 < y < len(field) - 1
    return field[y-1][x] + field[y][x+1] + field[y+1][x] + field[y][x-1]


def apply_rules(prev_field: typing.List[typing.List[str]], field: typing.List[typing.List[str]]):
    for y in range(1, len(field) - 1):
        for x in range(1, len(field) - 1):
            neighbors = get_adjacent(prev_field, x, y)
            bug_count = neighbors.count(BUG)
            if prev_field[y][x] == BUG:
                field[y][x] = EMPTY if bug_count != 1 else BUG
            elif prev_field[y][x] == EMPTY:
                field[y][x] = BUG if bug_count in (1, 2) else EMPTY


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()

    with open(args.filename) as fp:
        lines = fp.read().splitlines(keepends=False)
        field = [(len(lines) + 2) * [EMPTY] for _ in range(len(lines) + 2)]
        for y in range(len(lines)):
            for x in range(len(lines)):
                field[y+1][x+1] = lines[y][x]
        prev_field = copy.deepcopy(field)

    states = dict()
    states[tuple(tuple(row) for row in prev_field)] = 0
    print_field(prev_field)

    for minute in itertools.count():
        apply_rules(prev_field, field)
        field_tuple = tuple(tuple(row) for row in field)
        ranking = calc_biodiversity(field)
        print('After {} minutes'.format(minute + 1))
        print_field(field)
        print('Ranking:', ranking)
        print()
        if field_tuple in states:
            print('Part 1: Reached a same state after {} minutes. Ranking: {}'.format(minute + 1, ranking))
            print('Previous same state was after {} minutes'.format(states[field_tuple] + 1))
            break
        states[field_tuple] = minute
        prev_field, field = field, prev_field


if __name__ == '__main__':
    main()

# 2147484672 too high (after 108 minutes)
# 7558597376 was wrong. whoops
