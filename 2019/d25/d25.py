import argparse
import asyncio
import collections
import itertools
import typing

from d09 import d09


class Move(typing.NamedTuple):
    room_name: str
    door: str


def opposite_door(door: str) -> str:
    doors = ('north', 'east', 'south', 'west')
    door_i = doors.index(door)
    return doors[(door_i + 2) % len(doors)]


async def run_phase_1(queue_in: asyncio.Queue, queue_out: asyncio.Queue, event_in: asyncio.Event):
    BLACKLIST = (
        'infinite loop',
        'photons',
        'giant electromagnet',
        'escape pod',
        'molten lava',
    )

    is_parsing_doors = False
    is_parsing_items = False
    inventory = []
    move_stack = []
    goal_path = tuple()
    room_name = ''
    room_items = collections.defaultdict(set)
    room_doors = collections.defaultdict(set)
    visited_doors = collections.defaultdict(set)
    while True:
        line = (await d09.get_line(queue_out))[0]
        #print([line.rstrip('\n')])
        if line.startswith('Command?'):
            #print('Room name {}: doors {}, items {}'.format(room_name, room_doors, room_items))
            #print('Move stack: {}'.format(move_stack))
            #print('Visited doors of current room: {}'.format(visited_doors[room_name]))
            if room_items[room_name]:
                item = room_items[room_name].pop()
                #print('Taking item {}'.format(item))
                inventory.append(item)
                await d09.put_ascii(queue_in, event_in, 'take {}\n'.format(item))
            else:
                if move_stack:
                    prev_move = move_stack[-1]
                    visited_doors[room_name].add(opposite_door(prev_move.door))
                for door in room_doors[room_name]:
                    if door not in visited_doors[room_name]:
                        move_stack.append(Move(room_name, door))
                        #print('Going {}'.format(door))
                        visited_doors[room_name].add(door)
                        await d09.put_ascii(queue_in, event_in, '{}\n'.format(door))
                        break
                else:
                    if move_stack:
                        prev_move = move_stack.pop()
                        #print('Visited all doors, going back ({})'.format(prev_move.door))
                        await d09.put_ascii(queue_in, event_in, '{}\n'.format(opposite_door(prev_move.door)))
                    else:
                        print('Visited everything?')
                        print('Inventory: {}'.format(inventory))
                        break
        elif line.startswith('=='):
            room_name = line.strip().strip('=').strip()
            if room_name == 'Pressure-Sensitive Floor':
                goal_path = tuple(move_stack)
        elif line.startswith('Doors here lead'):
            is_parsing_doors = True
        elif line.startswith('Items here'):
            is_parsing_items = True
        elif line.startswith('- '):
            if is_parsing_doors:
                door = line.rstrip()[2:]
                room_doors[room_name].add(door)
            if is_parsing_items:
                item = line.rstrip()[2:]
                if item not in BLACKLIST:
                    room_items[room_name].add(item)
        else:
            is_parsing_doors = False
            is_parsing_items = False
    return tuple(inventory), goal_path


async def run_phase_2(initial_inventory: typing.Tuple[str], goal_path: typing.Tuple[Move],
                      queue_in: asyncio.Queue, queue_out: asyncio.Queue, event_in: asyncio.Event):
    print('### Phase 2')
    last_move = goal_path[-1]
    target_inventories = itertools.chain.from_iterable(
            itertools.combinations(initial_inventory, n + 1)
            for n in range(len(initial_inventory))
    )
    target_inventory = set(next(target_inventories))
    inventory = set(initial_inventory)
    path_remaining = list(reversed(goal_path))
    await d09.put_ascii(queue_in, event_in, '{}\n'.format(path_remaining.pop().door))  # The program expects a command
    while True:
        line = (await d09.get_line(queue_out))[0]
        print('>', line.rstrip('\n'))
        if line.startswith('Command?'):
            if path_remaining:
                await d09.put_ascii(queue_in, event_in, '{}\n'.format(path_remaining.pop().door))
            else:
                # Whack the pressure plate with different combinations!
                print('Inventory: {}, Target inventory: {}'.format(inventory, target_inventory))
                if inventory != target_inventory:
                    for item in inventory:
                        if item not in target_inventory:
                            await d09.put_ascii(queue_in, event_in, 'drop {}\n'.format(item))
                            inventory.remove(item)
                            break
                    else:
                        for item in target_inventory:
                            if item not in inventory:
                                await d09.put_ascii(queue_in, event_in, 'take {}\n'.format(item))
                                inventory.add(item)
                                break
                else:
                    print('Now we should have the correct combination')
                    await d09.put_ascii(queue_in, event_in, '{}\n'.format(last_move.door))
                    target_inventory = set(next(target_inventories))


async def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()

    with open(args.filename) as fp:
        program = d09.lexer(fp.read().strip())

    queue_in = asyncio.Queue(maxsize=1)
    queue_out = asyncio.Queue(maxsize=1)
    event_in = asyncio.Event()
    asyncio.create_task(d09.eval_program(list(program) + [0]*1000, queue_in, queue_out, event_in))

    inventory, goal_path = await run_phase_1(queue_in, queue_out, event_in)
    await run_phase_2(inventory, goal_path, queue_in, queue_out, event_in)

if __name__ == '__main__':
    asyncio.run(main())
