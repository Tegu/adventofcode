import collections
import enum
import sys


def eprint(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)


class Color(enum.Enum):
    Black = 0
    White = 1


class TurnDirection(enum.Enum):
    Left = 0
    Right = 1


class Orientation(enum.IntEnum):
    North = 0
    East = 1
    South = 2
    West = 3


def turn(orientation, direction):
    delta = 1 if direction == TurnDirection.Right else -1
    return Orientation((orientation + delta) % len(Orientation))


def move(position, orientation):
    x, y = position
    if orientation == Orientation.North:
        return x, y - 1
    elif orientation == Orientation.South:
        return x, y + 1
    elif orientation == Orientation.East:
        return x + 1, y
    elif orientation == Orientation.West:
        return x - 1, y
    else:
        return ValueError('Invalid move orientation {}'.format(orientation))


def print_enum(enum_):
    print(enum_.value)


def read_enum(enum_type):
    return enum_type(int(input()))


def eprint_painted(painted):
    left = min(painted.keys(), key=lambda p: p[0])[0]
    right = max(painted.keys(), key=lambda p: p[0])[0]
    top = min(painted.keys(), key=lambda p: p[1])[1]
    bottom = max(painted.keys(), key=lambda p: p[1])[1]
    for y in range(top, bottom+1):
        for x in range(left, right+1):
            color = painted.get((x, y), Color.Black)
            eprint('.' if color == Color.Black else '#', end='')
        eprint()



painted = collections.defaultdict(lambda: Color.Black)
position = (0, 0)
orientation = Orientation.North

painted[(0, 0)] = Color.White  # Part 2: start with single white tile

while True:
    panel = painted.get(position, Color.Black)
    print_enum(panel)
    try:
        color = read_enum(Color)
        direction = read_enum(TurnDirection)
    except EOFError:
        break
    except ValueError:
        print('Invalid input')
        sys.exit(1)

    painted[position] = color
    orientation = turn(orientation, direction)
    position = move(position, orientation)
    #eprint('New position {} and orientation {}'.format(position, repr(orientation)))


eprint('Part 1: Painted panels:', len(painted))
eprint_painted(painted)
