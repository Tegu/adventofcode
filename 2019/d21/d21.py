import argparse
import asyncio

from d09 import d09


async def put_ascii(queue: asyncio.Queue, text: str):
    for char in text:
        await queue.put(ord(char))


async def get_line(queue: asyncio.Queue):
    chars = []
    while True:
        num = await queue.get()
        if num >= 128:
            return ''.join(chars), num
        char = chr(num)
        chars.append(char)
        if char == '\n':
            break
    return ''.join(chars), None


async def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()

    with open(args.filename) as fp:
        program = d09.lexer(fp.read()) + [0]*100

    queue_in = asyncio.Queue()
    queue_out = asyncio.Queue()
    asyncio.ensure_future(d09.eval_program(list(program), queue_in, queue_out))

    await put_ascii(queue_in, 'NOT A J\n')
    await put_ascii(queue_in, 'NOT B T\n')
    await put_ascii(queue_in, 'OR T J\n')
    await put_ascii(queue_in, 'NOT C T\n')
    await put_ascii(queue_in, 'OR T J\n')
    await put_ascii(queue_in, 'AND D J\n')
    await put_ascii(queue_in, 'WALK\n')

    while True:
        line, non_ascii = await get_line(queue_out)
        if not non_ascii:
            print('<- {}'.format(line.strip()))
            if '#@#' in line:
                # Fell to a hole :(
                break
        else:
            print('Hull damage measured! Damage:', non_ascii)
            break


if __name__ == '__main__':
    asyncio.run(main())
