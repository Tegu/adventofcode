import sys

def dprint(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)

def lexer(text):
    return list(map(int, text.strip().split(',')))

def split_digits(num, n):
    digits = [0]*n
    for i in range(n):
        digits[i] = num % 10
        num = num // 10
    return tuple(digits)

def parse_op_modes(digits):
    opcode = digits[0] + 10 * digits[1]
    modes = digits[2:]
    return opcode, modes

def resolve_parameters(program, params, modes):
    assert(len(params) <= len(modes))
    values = [0]*len(params)
    for i, param in enumerate(params):
        if modes[i] == 0:
            values[i] = program[param]
        elif modes[i] == 1:
            values[i] = param
    return values

def eval_program(program, pc=0):
    while True:
        value = program[pc]
        opcode, modes = parse_op_modes(split_digits(value, 5))
        if opcode == 99:
            return
        elif opcode == 1:
            assert(modes[2] == 0)
            params = program[pc+1:pc+4]
            values = resolve_parameters(program, params, modes)
            program[params[2]] = values[0] + values[1]
            pc += 4
        elif opcode == 2:
            assert(modes[2] == 0)
            params = program[pc+1:pc+4]
            values = resolve_parameters(program, params, modes)
            program[params[2]] = values[0] * values[1]
            pc += 4
        elif opcode == 3:
            input_value = int(sys.stdin.readline().strip())
            output_location = program[pc+1]
            program[output_location] = input_value
            pc += 2
        elif opcode == 4:
            if modes[0] == 0:
                input_location = program[pc+1]
                input_value = program[input_location]
            elif modes[0] == 1:
                input_value = program[pc+1]
            sys.stdout.write('{}\n'.format(input_value))
            pc += 2
        elif opcode == 5:
            params = program[pc+1:pc+3]
            values = resolve_parameters(program, params, modes)
            if values[0] != 0:
                pc = values[1]
            else:
                pc += 3
        elif opcode == 6:
            params = program[pc+1:pc+3]
            values = resolve_parameters(program, params, modes)
            if values[0] == 0:
                pc = values[1]
            else:
                pc += 3
        elif opcode == 7:
            params = program[pc+1:pc+4]
            values = resolve_parameters(program, params, modes)
            program[params[2]] = int(values[0] < values[1])
            pc += 4
        elif opcode == 8:
            params = program[pc+1:pc+4]
            values = resolve_parameters(program, params, modes)
            program[params[2]] = int(values[0] == values[1])
            pc += 4
        else:
            print('Encountered unknown opcode {} at {}'.format(opcode, pc), file=sys.stderr)
            return


if __name__ == '__main__':
    if len(sys.argv) == 1:
        program = lexer(sys.stdin.readline())
    if len(sys.argv) == 2:
        program = lexer(open(sys.argv[1]).read())
    else:
        print('Program not found', file=sys.stderr)
        sys.exit(1)
    eval_program(program)
