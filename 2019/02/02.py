import sys

def restore_gravity_assist(program):
    program[1] = 12
    program[2] = 2

def lexer(text):
    return list(map(int, text.strip().split(',')))

def eval_program(program, pc=0):
    while True:
        opcode = program[pc]
        if opcode == 99:
            return
        input_locations = [program[pc+1], program[pc+2]]
        output_location = program[pc+3]
        inputs = [program[input_locations[0]], program[input_locations[1]]]
        if opcode == 1:
            program[output_location] = sum(inputs)
        elif opcode == 2:
            program[output_location] = inputs[0] * inputs[1]
        else:
            print('Encountered unknown opcode {} at {}'.format(opcode, pc), file=sys.stderr)
            return
        pc += 4


stop = False
initial_program = lexer(sys.stdin.read())
for noun in range(100):
    for verb in range(100):
        program = initial_program[:]
        program[1] = noun
        program[2] = verb
        eval_program(program)
        if program[0] == 19690720:
            print(noun, verb, program[0])
            stop = True
            break
    if stop:
        break
