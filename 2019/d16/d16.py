import sys

import numpy

signal_in = numpy.array([int(c) for c in sys.stdin.read().strip()])

size = len(signal_in)
base = numpy.array([0, 1, 0, -1])

patterns = numpy.array(list(
    numpy.tile(
        numpy.repeat(base, i),
        reps=size // len(base) + 1
        )[1:size+1]
    for i in range(1, size+1)))

signal = numpy.array(signal_in)
phases = int(sys.argv[1])
for _ in range(phases):
    signal = numpy.abs(numpy.matmul(patterns, signal)) % 10

print(''.join(str(n) for n in signal[:8]))
